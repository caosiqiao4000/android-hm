package gz.easyjf.color8.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
/**
 * 单位转换
 * @author MJ
 */
public class UnitUtil {
	/**
	 * 将dip转为px
	 */
	public static int dip2px(Context context, float dipValue){
		if(context!=null) {
			final float scale = context.getResources().getDisplayMetrics().density;
			return (int)(dipValue * scale + 0.5f);
		}
		return (int)dipValue;
    }  
    
	/**
	 * 将px转为dip
	 */
	public static int px2dip(Context context, float pxValue){  
		if(context!=null) {
			 final float scale = context.getResources().getDisplayMetrics().density;  
		     return (int)(pxValue / scale + 0.5f);  
		}
		return (int)pxValue;
    }  
	
	/**
	 * 将drawable转为bitmap
	 */
	public static Bitmap drawable2bitmap(Drawable d) {
		return ((BitmapDrawable)d).getBitmap();
	}
	
	/**
	 * 将bitmap转为drawable
	 */
	public static Drawable bitmap2drawable(Bitmap b) {
		return (Drawable)new BitmapDrawable(b);
	}
}
