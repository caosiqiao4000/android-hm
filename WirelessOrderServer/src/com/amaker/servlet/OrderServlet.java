package com.amaker.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amaker.dao.IOrderDao;
import com.amaker.dao.impl.OrderDaoImpl;
import com.amaker.entity.Order;
import com.amaker.entity.OrderDetail;

public class OrderServlet extends HttpServlet {

	private IOrderDao orderDao = new OrderDaoImpl();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		
		String userinfo = request.getParameter("userinfo");
		String[] strs = userinfo.split(":");
		String selectedTable = request.getParameter("selectedTable");
		String items = request.getParameter("items");
		String customerQuantity = request.getParameter("customerQuantity");
		
		/*System.out.println("userinfo==>"+userinfo);
		System.out.println("selectedTable==>"+selectedTable);
		System.out.println("items==>"+items);
		System.out.println("customerQuantity==>"+customerQuantity);*/
		
		Order order = new Order();
		order.setOrderTime(new Timestamp(new Date().getTime()));
		order.setUserId(Integer.parseInt(strs[0]));
		order.setTableId(Integer.parseInt(selectedTable));
		order.setPersonNum(Integer.parseInt(customerQuantity));
		order.setIsPay("0");
		order.setRemark("");
		
		String[] strItems = items.split(";");
		for(String item:strItems){
			String[] strDetail = item.split(":");
			OrderDetail detail = new OrderDetail();
			detail.setMenuId(Integer.parseInt(strDetail[0]));
			detail.setMenuName(strDetail[1]);
			detail.setNum(Integer.parseInt(strDetail[2]));
			detail.setRemark("");
			order.getDetails().add(detail);
		}
		int orderId = orderDao.createOrder(order);
		out.println(String.valueOf(orderId));
		
		out.flush();
		out.close();
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doGet(request, response);
	}

}
