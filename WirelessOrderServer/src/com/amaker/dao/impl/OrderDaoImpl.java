package com.amaker.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.amaker.dao.IOrderDao;
import com.amaker.entity.Order;
import com.amaker.entity.OrderDetail;
import com.amaker.util.DBUtil;

public class OrderDaoImpl implements IOrderDao {

	public int createOrder(Order order) {
		// TODO Auto-generated method stub
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		Statement stm = null;
		int orderId = -1;
		try {
			con = DBUtil.openConnection();
			con.setAutoCommit(false);
			String sql = "insert into ordertbl(orderTime,userId,tableId,personNum,isPay,remark) values(?,?,?,?,?,?)";
			ps = con.prepareStatement(sql);
			ps.setTimestamp(1, order.getOrderTime());
			ps.setInt(2, order.getUserId());
			ps.setInt(3, order.getTableId());
			ps.setInt(4, order.getPersonNum());
			ps.setString(5, order.getIsPay());
			ps.setString(6, order.getRemark());
			ps.execute();
			ps.close();
			
			String maxIdSQL = "select max(id) max_id from ordertbl";
			stm = con.createStatement();
			rs = stm.executeQuery(maxIdSQL);
			
			if(rs.next()){
				orderId = rs.getInt("max_id");
			}
			
			if(orderId!=-1){
				sql= "insert into orderdetailtbl(orderId,menuId,menuname,num,remark) values(?,?,?,?,?)";
				ps = con.prepareStatement(sql);
				for(OrderDetail detail:order.getDetails()){
					ps.setInt(1, orderId);
					ps.setInt(2, detail.getMenuId());
					ps.setString(3, detail.getMenuName());
					ps.setInt(4, detail.getNum());
					ps.setString(5, "");
					ps.addBatch();
				}
				int[] nums = ps.executeBatch();
				System.out.println(nums.length+"行执行成功");
			}
			
			//更新餐桌状态
			sql = "update tabletbl set flag='1' where id="+order.getTableId();
			stm.execute(sql);
			con.setAutoCommit(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}finally{
			DBUtil.close(null, ps, null);
			DBUtil.close(rs, stm, con);
		}
		
		
		return orderId;
	}

}
