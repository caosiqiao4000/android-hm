package com.amaker.dao;

import java.util.List;

import com.amaker.entity.Menu;

public interface IMenuDao {
//	查找所有餐桌信息
	List<Menu> findAllMenu();
}
