package com.amaker.dao;

import com.amaker.entity.Order;

public interface IOrderDao {
	public int createOrder(Order order);
}
