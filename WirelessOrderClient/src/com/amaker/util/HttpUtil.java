package com.amaker.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.amaker.entity.CheckTable;

public class HttpUtil {

	public static String base_url = "http://10.0.2.2:8080/WirelessOrderServer/";
	private static DefaultHttpClient client = new DefaultHttpClient();
	private HttpUtil(){
		
	}
	
	// 获得Post请求对象request
	public static HttpPost getHttpPost(String url){
		 HttpPost request = new HttpPost(url);
		 return request;
	}
	
	public static String queryForPost(HttpPost request){
		String result = "-1";
		try {
			HttpResponse response = client.execute(request);
			if(response.getStatusLine().getStatusCode()==200){
				if(response.getStatusLine().getStatusCode()==200){
					result = EntityUtils.toString(response.getEntity());
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			result = "网络异常";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "网络异常";
		}
		return result;
	}
	
	//获得所有的餐桌信息s
	public static List<CheckTable> getTables(){
		List<CheckTable> cts = new ArrayList<CheckTable>();
		StringBuilder url = new StringBuilder();
		url.append(HttpUtil.base_url+"CheckTableServlet");
		HttpPost request = getHttpPost(url.toString());
		String result = HttpUtil.queryForPost(request);
		String[] tables = result.trim().split(";");
		for(String table:tables){
			String[] strs = table.split(":");
			CheckTable ct = new CheckTable();
			ct.setNum(Integer.parseInt(strs[0]));
			ct.setFlag(strs[1]);
			cts.add(ct);
		}
		return cts;
	}
}
