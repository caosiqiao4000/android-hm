package com.amaker.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amaker.entity.CheckTable;
import com.amaker.util.HttpUtil;

public class CheckTableActivity extends Activity {

	private List<CheckTable> cts = new ArrayList<CheckTable>();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.checktable);
		cts = HttpUtil.getTables();
		GridView checkTableGridView = (GridView)this.findViewById(R.id.checkTableGridView);
		checkTableGridView.setAdapter(new ImageAdater(this));
	}
	
	
	
	private class ImageAdater extends BaseAdapter{
		private Context context;
		public ImageAdater(Context context){
			this.context = context;
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return cts.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			View view = null;
			if(convertView==null){
				view = LayoutInflater.from(context).inflate(R.layout.checktable_item, null);
				ImageView iv = (ImageView)view.findViewById(R.id.ivCheckTable);
				TextView tv = (TextView)view.findViewById(R.id.tvCheckTable);
				CheckTable ct = cts.get(position);
				tv.setText(ct.getNum()+"");
				if(ct.getFlag().equals("0")){
					iv.setImageResource(R.drawable.kongwei);
				}else{
					iv.setImageResource(R.drawable.youren);
				}
				
			}else{
				view = convertView;
			}
			
			return view;
		}
		
	}
	
}
