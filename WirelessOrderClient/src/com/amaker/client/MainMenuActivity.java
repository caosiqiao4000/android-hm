package com.amaker.client;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.amaker.entity.CheckTable;
import com.amaker.util.HttpUtil;

public class MainMenuActivity extends Activity {

	private Integer[] imgs = { R.drawable.chatai, R.drawable.bingtai,
			R.drawable.zhuantai, R.drawable.diancai, R.drawable.gengxin,
			R.drawable.shezhi, R.drawable.zhuxiao, R.drawable.jietai };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.mainmenu);
		GridView actionGridView = (GridView) this
				.findViewById(R.id.actionGridView);
		actionGridView.setAdapter(new ImageAdater(this));
	}

	
	private class ImageAdater extends BaseAdapter {

		private Context context;
		public ImageAdater(Context context){
			this.context = context;
		}
		
		//确定GridView里面有多少个网格
		/*
		 *int count = getCount();
		 *for(int i=0;i<count;i++){
		 *	View view = getView(i,...,...);
		 *}
		 */
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return imgs.length;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		//每个网络会显示的视图
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			ImageView imageView = null;
			if(convertView==null){
				imageView = new ImageView(context);
				imageView.setImageResource(imgs[position]);
			}else{
				imageView = (ImageView)convertView;
			}
			switch(position){
			case 0:
				imageView.setOnClickListener(checkTableListener);
				break;
			case 1:
				imageView.setOnClickListener(unionTableListener);
				break;
			case 2:
				break;
			case 3:
				imageView.setOnClickListener(orderListener);
				break;
			}
			
			return imageView;
		}
	}

	//查台监听器
	private OnClickListener checkTableListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(MainMenuActivity.this,CheckTableActivity.class);
			MainMenuActivity.this.startActivity(intent);
		}
	};
	
	//查台监听器
	private OnClickListener orderListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(MainMenuActivity.this,OrderActivity.class);
			MainMenuActivity.this.startActivity(intent);
		}
	};
	
	//并台监听器
	private OnClickListener unionTableListener = new OnClickListener() {
		
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			AlertDialog.Builder dialog = new AlertDialog.Builder(MainMenuActivity.this);
			dialog.setTitle("合并桌位");
			LayoutInflater inflater = LayoutInflater.from(MainMenuActivity.this);
			View view = inflater.inflate(R.layout.union_dialog, null);
			final Spinner sourceTableSpinner = (Spinner)view.findViewById(R.id.dialog_unionSourceTable);
			final Spinner destTableSpinner = (Spinner)view.findViewById(R.id.dialog_unionDestTable);
			
			List<CheckTable> cts = HttpUtil.getTables();
			List<Integer> datas = new ArrayList<Integer>();
			for(CheckTable ct:cts){
				if(ct.getFlag().equals("1")){
					datas.add(ct.getNum());
				}
			}
			
			ArrayAdapter<Integer> sourceAdapater = new ArrayAdapter<Integer>(MainMenuActivity.this,android.R.layout.simple_spinner_dropdown_item,datas);
			ArrayAdapter<Integer> destAdapater = new ArrayAdapter<Integer>(MainMenuActivity.this,android.R.layout.simple_spinner_dropdown_item,datas);
			
			sourceTableSpinner.setAdapter(sourceAdapater);
			destTableSpinner.setAdapter(destAdapater);
			
			dialog.setView(view);
			dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String sourceTable = sourceTableSpinner.getSelectedItem().toString();
					String destTable = destTableSpinner.getSelectedItem().toString();
					if(sourceTable.equals(destTable)){
						Toast.makeText(MainMenuActivity.this, "被并桌位和目标桌位不能一样", Toast.LENGTH_LONG).show();
						return;
					}
					List<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair("sourceTable", sourceTable));
					params.add(new BasicNameValuePair("destTable", destTable));
					
					try {
						UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params,HTTP.UTF_8);
						StringBuilder url = new StringBuilder(HttpUtil.base_url+"UnionTableServlet");
						HttpPost request = HttpUtil.getHttpPost(url.toString());
						request.setEntity(entity);
						String result = HttpUtil.queryForPost(request);
						Toast.makeText(MainMenuActivity.this, result, Toast.LENGTH_LONG).show();
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			});
			dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			});
			dialog.show();
		}
	};
}
