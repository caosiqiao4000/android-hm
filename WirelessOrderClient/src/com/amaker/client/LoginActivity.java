package com.amaker.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.amaker.util.HttpUtil;

public class LoginActivity extends Activity implements OnClickListener{

	private EditText etAccount;
	private EditText etPassword;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        loadView();
    }
    
    private void loadView(){
    	etAccount = (EditText)this.findViewById(R.id.etLoginAccount);
    	etPassword = (EditText)this.findViewById(R.id.etLoginPassword);
    	View bnLogin = this.findViewById(R.id.bnLogin);
    	View bnReset = this.findViewById(R.id.bnReset);
    	bnLogin.setOnClickListener(this);
    	bnReset.setOnClickListener(this);
    }

	@Override
	public void onClick(View v) {
		if(v.getId()==R.id.bnLogin){
			String account = etAccount.getText().toString();
			String password = etPassword.getText().toString();
			if(account==null || account.trim().equals("")){
				prompt("用户名不能为空!!");
			}else if(password==null || password.trim().equals("")){
				prompt("密码不能为空!!");
			}else{
				StringBuilder url = new StringBuilder();
				url.append(HttpUtil.base_url+"LoginServlet?account="+account+"&password="+password);
				String result = HttpUtil.queryForPost(HttpUtil.getHttpPost(url.toString()));
				Log.d("debug", "**********result===>"+result);
				if(result.trim().equals("-1")){
					prompt("用户名或者密码有误");
				}else{
					//登录成功后，将用户信息保存到android客户端的login.xml文件中
					SharedPreferences sp = this.getSharedPreferences("login", Activity.MODE_WORLD_READABLE);
					Editor editor = sp.edit();
					editor.putString("userinfo", result);
					editor.commit();
					Intent intent = new Intent(this,MainMenuActivity.class);
					this.startActivity(intent);
				}
				
			}
		}else if(v.getId()==R.id.bnReset){
			
		}
		
	}
	
	private void prompt(String msg){
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("信息提示框");
		dialog.setMessage(msg);
		dialog.setPositiveButton("确定", null);
		dialog.show();
	}
	
}