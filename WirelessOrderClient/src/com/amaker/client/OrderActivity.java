package com.amaker.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.amaker.entity.CheckTable;
import com.amaker.util.HttpUtil;

public class OrderActivity extends ListActivity {

	private SAXReader reader = new SAXReader();

	private List<Map<String, Object>> datas = new ArrayList<Map<String, Object>>();

	private ListView listView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// this.setContentView(R.layout.order);
		initDatas();
		initView();
	}

	// 初始化数据
	private void initDatas() {
		StringBuilder sb = new StringBuilder();
		sb.append(HttpUtil.base_url + "MenuServlet");
		try {
			URL url = new URL(sb.toString());
			URLConnection conn = url.openConnection();
			InputStream is = conn.getInputStream();
			Document doc = reader.read(is);
			Element root = doc.getRootElement();
			List<Element> list = root.elements("menu");
			for (Element e : list) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("action", false);
				map.put("menuItemId",e.elementText("id") );
				map.put("menuItemNo", e.elementText("no"));
				map.put("menuItemName", e.elementText("name"));
				map.put("menuItemPrice", e.elementText("price"));
				map.put("menuItemQuantity", "0");
				datas.add(map);
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (DocumentException ex) {
			ex.printStackTrace();
		}
	}
	private SimpleAdapter adapter;
	// 初始化视图
	public void initView() {
		this.setTitle("菜单目录");
		// listView = (ListView) this.findViewById(R.id.menuListView);
		System.out.println("datas.size()===>" + datas.size());

		adapter = new SimpleAdapter(
				this,
				datas,
				R.layout.menu_item,
				new String[] { "action", "menuItemNo", "menuItemName",
						"menuItemPrice", "menuItemQuantity" },
				new int[] {R.id.cbSelectedMenu, R.id.tvMenuItemNO,
						R.id.tvMenuName, R.id.tvMenuPrice, R.id.tvMenuQuantity });

		// listView.setAdapter(adapter);
		this.setListAdapter(adapter);
		
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {

		final Map<String,Object> map = datas.get(position);
		boolean flag = (Boolean)map.get("action");
		if(!flag){
			map.put("action", true);
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle("你选择的菜单是："+map.get("menuItemName"));
			LayoutInflater flater = LayoutInflater.from(this);
			final View view = flater.inflate(R.layout.quantity_dialog, null);
			dialog.setView(view);
			dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					EditText etQuantity = (EditText)view.findViewById(R.id.dialog_etQuantity);
					map.put("menuItemQuantity",etQuantity.getText().toString());
					adapter.notifyDataSetChanged();
				}
			});
			dialog.show();
			
		}else{
			map.put("action", false);
			adapter.notifyDataSetChanged();
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("下单");
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("你确定要下单吗？");
		LayoutInflater inflater = LayoutInflater.from(this);
		final View view = inflater.inflate(R.layout.order_dialog, null);
		List<CheckTable> cts = HttpUtil.getTables();
		final Spinner tableSpinner = (Spinner)view.findViewById(R.id.dialog_tableSpinner);
		List<Integer> emtyTables = new ArrayList<Integer>();
		for(CheckTable ct:cts){
			if(ct.getFlag().equals("0")){
				emtyTables.add(ct.getNum());
			}
		}
		ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_dropdown_item,emtyTables);
		tableSpinner.setAdapter(arrayAdapter);
		
		final EditText etCustomerQuantity = (EditText)view.findViewById(R.id.dialog_etCustomerQuantity);
		dialog.setView(view);
		
		dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				SharedPreferences sp = OrderActivity.this.getSharedPreferences("login", Activity.MODE_WORLD_READABLE);
				//获得用户信息
				String infos = sp.getString("userinfo", null);
				//String[] strs = infos.split(":");
				//获得被选中的餐桌
				String selectedTable = tableSpinner.getSelectedItem().toString();
				//获得人数
				String customerQuantity = etCustomerQuantity.getText().toString();
				//获得选中菜谱 
				StringBuilder sb = new StringBuilder();
				for(Map<String, Object> map:datas){
					boolean flag = (Boolean)map.get("action");
					String menuItemQuantity = (String)map.get("menuItemQuantity");
					if(flag && Integer.parseInt(menuItemQuantity)>0){
						sb.append(map.get("menuItemId")+":"+map.get("menuItemName")+":"+map.get("menuItemQuantity")+";");
					}
				}
				//1:白斩鸡:3;2:白切鸡:2;
				sb.delete(sb.lastIndexOf(";"),sb.length());
				
				//将参数绑定到request,发送到服务器
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("userinfo", infos));
				params.add(new BasicNameValuePair("selectedTable", selectedTable));
				params.add(new BasicNameValuePair("customerQuantity", customerQuantity));
				params.add(new BasicNameValuePair("items", sb.toString()));
				
				try {
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params,HTTP.UTF_8);
					StringBuilder url = new StringBuilder();
					url.append(HttpUtil.base_url+"OrderServlet");
					HttpPost request = HttpUtil.getHttpPost(url.toString());
					request.setEntity(entity);
					String result = HttpUtil.queryForPost(request);
					Toast.makeText(OrderActivity.this, "订单号为："+result, Toast.LENGTH_LONG).show();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		});
		dialog.show();
		return super.onOptionsItemSelected(item);
	}
	
	
}