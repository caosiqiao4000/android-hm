create table MenuTypeTbl
(
   _id                   integer primary key  autoincrement,
   name                 varchar(20)
);

create table MenuTbl
(
   _id                   integer primary key autoincrement,
   typeId               integer,
   menuItemNO           varchar(20),
   name                 varchar(50),
   price                float,
   pic                  varchar(100),
   status               varchar(2),
   remark               varchar(200)
);


create table TableTbl
(
   _id                   integer primary key autoincrement,
   num                  integer,
   flag                 varchar(2),
   description          varchar(100)
);


#TableTbl����
insert into TableTbl values(1,1,'0','������');
insert into TableTbl values(2,2,'1','������');
insert into TableTbl values(3,3,'0','������');
insert into TableTbl values(4,4,'0','������');
insert into TableTbl values(5,5,'1','������');
insert into TableTbl values(6,6,'0','������');
insert into TableTbl values(7,7,'1','������');
insert into TableTbl values(8,8,'0','������');
select * from TableTbl;
#MenuTypeTbl����
insert into MenuTypeTbl(name) values('����');
insert into MenuTypeTbl(name) values('�Ȳ�');
insert into MenuTypeTbl(name) values('����');
insert into MenuTypeTbl(name) values('С��');
insert into MenuTypeTbl(name) values('��Ʒ');
insert into MenuTypeTbl(name) values('��ʳ');
insert into MenuTypeTbl(name) values('�����ʳ');
#MenuTbl����
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(1,'gz0101','��ն��',38,'images/codedish_baizanji.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(1,'gz0102','��ˮ��',25,'images/codedish_banshuimi.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(1,'gz0103','���趹��˿',18,'images/codedish_doufusi.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(1,'gz0104','��ݮɽҩ',32,'images/codedish_shanyao.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(1,'gz0105','��������',36,'images/codedish_shumayaohua.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(1,'gz0106','��������',45,'images/codedish_youyujuan.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(2,'gz0201','�������',20,'images/hotdish_bianchi.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(2,'gz0202','���㴺��',35,'images/hotdish_chunshun.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(2,'gz0203','�س��̶�ѿ',20,'images/hotdish_douya.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(2,'gz0204','����÷��',38,'images/hotdish_meiyu.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(2,'gz0205','�����ﵶ��',35,'images/hotdish_qiudaoyu.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(3,'gz0301','�μⲤ����',38,'images/porridge_bocaizhou.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(3,'gz0302','����ɽҩ��',28,'images/porridge_shanyaozhou.jpg','1','');
insert into MenuTbl(typeID,menuItemNo,name,price,pic,status,remark) values(3,'gz0303','��������з��',45,'images/porridge_xiezhou.jpg','1','');
select * from MenuTbl;