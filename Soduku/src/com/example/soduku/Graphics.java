package com.example.soduku;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.os.Bundle;
import android.view.View;
/*
 * 2D画图
 */
public class Graphics extends Activity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(new GraphicsView(this));
	}
	private static final String QUOTE = "Now is the time for all " +
	"good men to come to the aid of their country." ;
	static public class GraphicsView extends View {
		public GraphicsView(Context context) {
			super(context);
		}
		//在此方法画图
		public void onDraw(Canvas canvas) {
			// Drawing commands go here
			//可以利用Path类画圆、矩形、正方形等图形
			Path circle = new Path();
			//This defines a circle at position x=150, y=150, with a radius of 100 pixels.
			circle.addCircle(150, 150, 100, Direction.CW);
			
			//canvas.drawPath(circle, cPaint);
			//canvas.drawTextOnPath(QUOTE, circle, 0, 20, tPaint);
			
		}
	}
}
