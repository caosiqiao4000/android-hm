package com.example.soduku;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

public class Soduku extends Activity implements OnClickListener {
	/** Called when the activity is first created. */
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		View bnAbout = this.findViewById(R.id.bnAbout);
		View bnContinue = this.findViewById(R.id.bnContinue);
		View bnNewGame = this.findViewById(R.id.bnNewGame);
		View bnExit = this.findViewById(R.id.bnExit);

		// 增加事件监听
		bnAbout.setOnClickListener(this);
		bnContinue.setOnClickListener(this);
		bnNewGame.setOnClickListener(this);
		bnExit.setOnClickListener(this);

		// If the activity is restarted, do a continue next time
		//getIntent().putExtra(KEY_DIFFICULTY, DIFFICULTY_CONTINUE);
	}

	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.bnAbout: {
				Intent i = new Intent(this, About.class);
				this.startActivity(i);
				// 消毁当前窗口对象
				// this.finish();
				break;
			}
			case R.id.bnNewGame:{
				//弹出消息框
				AlertDialog.Builder ab = new AlertDialog.Builder(this);
				ab.setTitle(R.string.new_game_title).setItems(R.array.difficulty,new DialogInterface.OnClickListener(){

					public void onClick(DialogInterface dialoginterface,
							int i) {
							startGame(i);
					}
					
				}).show();
				break;
			}
			case R.id.bnExit:{
				this.finish();
				break;
			}
			case R.id.bnContinue:{
				startGame(Game.DIFFICULTY_CONTINUE);
				break;
			}
				
		}

	}
	private static final String TAG = "Sudoku" ;
	private void startGame(int i) {
		Log.d(TAG, "clicked on " + i);
		// Start game here...
		Intent intent = new Intent(this, Game.class);
		intent.putExtra(Game.KEY_DIFFICULTY, i);
		startActivity(intent);
	}
	/*
	 * 在窗口点击menu按钮时，将会调用该方法，getMenuInflater()返回在menu.xml定义的菜单对象,
	 * 当点击某个菜单项时，onOptionsItemSelected()将被调用
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			//单击setting菜单项时执行
			case R.id.settings:{
				Intent i = new Intent(this,Settings.class);
				this.startActivity(i);
				return true;
			}
		}
		return false;
	}
	//stop music
	public void onPause() {
		super.onPause();
		Music.stopMusic();
	}
	//start music
	public void onResume() {
		super.onResume();
		Music.startMusic(this,R.raw.one);
	}

}