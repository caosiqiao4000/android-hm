package com.example.soduku;

import android.content.Context;
import android.media.MediaPlayer;

public class Music {
	private static MediaPlayer mp;

	private Music() {
	}

	public static void startMusic(Context context, int resource) {
		mp = MediaPlayer.create(context, resource);
		mp.setLooping(true);
		mp.start();
	}

	public static void stopMusic() {
		if (mp != null) {
			// stops the music
			mp.stop();
			// The release( ) method frees system resources associated with the
			// player
			mp.release();
			mp = null;
		}
	}

	public static void play(Context context, int resource) {
		stopMusic();
		// Start music only if not disabled in preferences
		if (Settings.getMusic(context)) {
			mp = MediaPlayer.create(context, resource);
			mp.setLooping(true);
			mp.start();
		}
		/*if (Settings.getHints(getContext())) {
			// Draw the hints...
		}*/
	}
}
