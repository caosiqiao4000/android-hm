package com.example.soduku;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

//PreferencesActivity是Android 中专门用来实现程序设置界面及参数存储的一个Activity
/*PreferencesActivity常见的几个组件：
 * PreferenceScreen：设置页面，可嵌套形成二级设置页面，用Title参数设置标题。
 * PreferenceCategory：某一类相关的设置，可用Title参数设置标题。
 * CheckBoxPreference：是一个CheckBox设置，只有两种值，true或false，
 * 可用Title参数设置标题，用summaryOn和summaryOff参数来设置控件选中和未选中时的提示。
 */
public class Settings extends PreferenceActivity {
	// Option names and default values
	private static final String OPT_MUSIC = "music";
	private static final boolean OPT_MUSIC_DEF = true;
	private static final String OPT_HINTS = "hints";
	private static final boolean OPT_HINTS_DEF = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*
		 * The addPreferencesFromResource( ) method reads the settings
		 * definition from XML and inflates it into views in the current
		 * activity. All the heavy lifting takes place in the PreferenceActivity
		 * class.
		 */
		addPreferencesFromResource(R.xml.settings);
	}

	/** Get the current value of the music option */
	public static boolean getMusic(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean(OPT_MUSIC, OPT_MUSIC_DEF);
	}

	/** Get the current value of the hints option */
	public static boolean getHints(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean(OPT_HINTS, OPT_HINTS_DEF);
	}

	
}
