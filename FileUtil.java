package gz.easyjf.color8.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
/**
 * 用于文件操作
 * @author MJ
 *
 */
public class FileUtil {
	/**
	 * 创建新的文件
	 * @param dirPath 文件目录
	 * @param name 文件名
	 */
	public static File createFileInSDCard(String dirPath, String name)
			throws Exception {
		File file = new File(dirPath, name);
		file.createNewFile();
		return file;
	}
	
	/**
	 * 创建新目录
	 */
	public static File creatSDDir(String dirPath) {
		File dirFile = new File(dirPath);
		if(!dirFile.exists()) dirFile.mkdirs();
		return dirFile;
	}
	
	/**
	 * 判断文件存不存在
	 */
	public static boolean isFileExist(String name) {
		return new File(name).exists();
	}
	
	/**
	 * 将流转化成文件，写进内存卡
	 */
	public static File write2SDFromInput(String name, InputStream input) {
		File file = null;
		OutputStream output = null;
		try {
			int index = name.lastIndexOf("/");
			String dir = name.substring(0, index);
			creatSDDir(dir);
			file = createFileInSDCard(dir, name.substring(index+1));
			output = new FileOutputStream(file);
			byte buffer[] = new byte[4 * 1024];
			int temp;
			while ((temp = input.read(buffer)) != -1) {
				output.write(buffer, 0, temp);
			}
			output.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				output.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return file;
	}
}
