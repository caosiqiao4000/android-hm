/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2010-3-30 15:38:16                           */
/*==============================================================*/


drop table if exists MenuTbl;

drop table if exists MenuTypeTbl;

drop table if exists OrderDetailTbl;

drop table if exists OrderTbl;

drop table if exists TableTbl;

drop table if exists UserTbl;

/*==============================================================*/
/* Table: MenuTbl                                               */
/*==============================================================*/
create table MenuTbl
(
   id                   integer not null auto_increment comment '主键，主动增加',
   typeId               integer comment '菜谱类别,外键，引用菜单类型表ID',
   menuItemNO           varchar(20) comment '菜单编号',
   name                 varchar(50) comment '名称',
   price                float comment '价格',
   pic                  varchar(100) comment '图片路径',
   status               varchar(2) comment '菜单状态',
   remark               varchar(200) comment '备注',
   primary key (id)
);

alter table MenuTbl comment '菜谱表';

/*==============================================================*/
/* Table: MenuTypeTbl                                           */
/*==============================================================*/
create table MenuTypeTbl
(
   id                   integer not null auto_increment comment '主键,自动增加',
   name                 varchar(20) comment '类型名称',
   primary key (id)
);

alter table MenuTypeTbl comment '菜谱分类表';

/*==============================================================*/
/* Table: OrderDetailTbl                                        */
/*==============================================================*/
create table OrderDetailTbl
(
   id                   integer not null auto_increment comment '主键，自动增加',
   orderId              integer comment '外键，参照订单Id',
   menuId               integer comment '外键,参照菜谱Id',
   menuname             varchar(50) comment '菜谱名称',
   num                  integer comment '数量',
   remark               varchar(200) comment '备注',
   primary key (id)
);

alter table OrderDetailTbl comment '订单明细表';

/*==============================================================*/
/* Table: OrderTbl                                              */
/*==============================================================*/
create table OrderTbl
(
   id                   integer not null auto_increment comment '主键,自动增加',
   orderTime            timestamp comment '下单时间',
   userId               integer comment '下单用户',
   tableId              integer comment '桌号，外键引用桌号id',
   personNum            integer comment '人数',
   isPay                varchar(2) comment '是否结算,0：未结算;1:结算',
   remark               varchar(200) comment '备注',
   primary key (id)
);

alter table OrderTbl comment '订单表';

/*==============================================================*/
/* Table: TableTbl                                              */
/*==============================================================*/
create table TableTbl
(
   id                   integer not null auto_increment comment 'id',
   num                  integer comment '桌号',
   flag                 varchar(2) comment '餐桌状态 0:空桌;1：有人就餐',
   description          varchar(100) comment '描述',
   primary key (id)
);

alter table TableTbl comment '餐桌表';

/*==============================================================*/
/* Table: UserTbl                                               */
/*==============================================================*/
create table UserTbl
(
   id                   integer not null auto_increment comment '主键，自动增加',
   account              varchar(20) comment '登录帐号',
   password             varchar(20) comment '登录密码',
   name                 varchar(20) comment '姓名',
   gender               varchar(20) comment '性别',
   permission           varchar(2) comment '权限：1,管理员;2,收银员;3,服务员',
   remark               varchar(200) comment '备注',
   primary key (id)
);

alter table UserTbl comment '系统用户表';

alter table MenuTbl add constraint FK_Reference_1 foreign key (typeId)
      references MenuTypeTbl (id) on delete restrict on update restrict;

alter table OrderDetailTbl add constraint FK_Reference_4 foreign key (orderId)
      references OrderTbl (id) on delete restrict on update restrict;

alter table OrderDetailTbl add constraint FK_Reference_5 foreign key (menuId)
      references MenuTbl (id) on delete restrict on update restrict;

alter table OrderTbl add constraint FK_Reference_2 foreign key (userId)
      references UserTbl (id) on delete restrict on update restrict;

alter table OrderTbl add constraint FK_Reference_3 foreign key (tableId)
      references TableTbl (id) on delete restrict on update restrict;

