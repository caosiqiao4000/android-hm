package com.amaker.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amaker.dao.IUserDao;
import com.amaker.dao.impl.UserDaoImpl;
import com.amaker.entity.User;

public class LoginServlet extends HttpServlet {

	private IUserDao userDao = new UserDaoImpl();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		User user = userDao.login(account, password);
		if(user==null){
			out.println("-1");
		}else{
			out.println(user.getId()+":"+user.getAccount()+":"+user.getName()+":"+user.getPermission());
		}
		out.flush();
		out.close();
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}

}
