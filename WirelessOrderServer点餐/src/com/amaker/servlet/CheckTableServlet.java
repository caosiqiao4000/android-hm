package com.amaker.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amaker.dao.ITableDao;
import com.amaker.dao.impl.TableDaoImpl;
import com.amaker.entity.CheckTable;

public class CheckTableServlet extends HttpServlet {

	private ITableDao tableDao = new TableDaoImpl();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("********CheckTableServlet's doGet().....");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		List<CheckTable> list = tableDao.findAllTable();
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<list.size();i++){
			if(i<list.size()-1){
				sb.append(list.get(i).getNum()+":"+list.get(i).getFlag()+";");
			}else{
				sb.append(list.get(i).getNum()+":"+list.get(i).getFlag());
			}
		}
		System.out.println("拼凑之后的结果*****"+sb.toString());
		//拼凑之后的结果： 1:0;2:1;3:0
		out.println(sb.toString());
		out.flush();
		out.close();
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doGet(request, response);
	}

}
