package com.amaker.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amaker.dao.IMenuDao;
import com.amaker.dao.impl.MenuDaoImpl;
import com.amaker.entity.Menu;

public class MenuServlet extends HttpServlet {

	private IMenuDao menuDao = new MenuDaoImpl();
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("MenuServlet's doGet()...........");
		response.setContentType("text/xml;charset=utf-8");
		PrintWriter out = response.getWriter();
		List<Menu> list = menuDao.findAllMenu();
//		 拼XML格式数据
		out.println("<?xml version='1.0' encoding='UTF-8'?>");
		// 根节点
		out.println("<menulist>");
			for (int i = 0; i <list.size(); i++) {
				Menu m = (Menu)list.get(i);
				out.println("<menu>");
					// 菜谱编号
					out.print("<id>");
						out.print(m.getId());
					out.println("</id>");
					// 分类
					out.print("<typeId>");
						out.print(m.getTypeId());
					out.println("</typeId>");
//					 编号
					out.print("<no>");
						out.print(m.getMenuItemNo());
					out.println("</no>");
					// 名称
					out.print("<name>");
						out.print(m.getName());
					out.println("</name>");
					// 图片路径
					out.print("<pic>");
						out.print(m.getPic());
					out.println("</pic>");
					// 价格
					out.print("<price>");
						out.print(m.getPrice());
					out.println("</price>");
					// 备注
					out.print("<remark>");
						out.print(m.getRemark());
					out.println("</remark>");
					
				out.println("</menu>");
			}
		out.println("</menulist>");
		out.flush();
		out.close();
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doGet(request, response);
	}

}
