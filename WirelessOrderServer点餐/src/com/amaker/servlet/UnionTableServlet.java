package com.amaker.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amaker.dao.ITableDao;
import com.amaker.dao.impl.TableDaoImpl;

public class UnionTableServlet extends HttpServlet {

	private ITableDao tableDao = new TableDaoImpl();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		try{
			String sourceTableId = request.getParameter("sourceTable");
			String destTableId = request.getParameter("destTable");
			tableDao.updateStatus(Integer.parseInt(sourceTableId), Integer.parseInt(destTableId));
			out.println("合并成功");
		}catch(Exception ex){
			ex.printStackTrace();
			out.println("合并失败");
		}
		out.flush();
		out.close();
	}

	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doGet(request, response);
	}

}
