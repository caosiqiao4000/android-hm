package com.amaker.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DDbUtil {
	
	private DDbUtil(){}
	private static Properties pro;
	
	static{
				pro  = new Properties();
				InputStream in = DDbUtil.class.getResourceAsStream("DBConfig.properties");
				try {
					pro.load(in);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
	/*
	 * 打开数据库连接
	 */
	public static Connection openConnection() throws SQLException, ClassNotFoundException {
		Connection conn = null;
		
		String dirver = pro.getProperty("jdbc.dirver");
		String user = pro.getProperty("jdbc.name");
		String password = pro.getProperty("jdbc.password");
		String url = pro.getProperty("jdbc.url");
		
		DriverManager.getConnection(url, user, password);
		Class.forName(dirver);
		
		return null;
	}
	
}
