package com.amaker.dao;

import java.util.List;

import com.amaker.entity.CheckTable;

public interface ITableDao {
	
	//查找所有餐桌信息
	List<CheckTable> findAllTable();
	
	//更新桌位状态
	void updateStatus(int sourceTableId,int destTableId) throws Exception;
}
