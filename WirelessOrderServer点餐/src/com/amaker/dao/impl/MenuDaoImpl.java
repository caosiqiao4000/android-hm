package com.amaker.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.amaker.dao.IMenuDao;
import com.amaker.entity.Menu;
import com.amaker.util.DBUtil;

public class MenuDaoImpl implements IMenuDao {

	public List<Menu> findAllMenu() {
		Connection con = null;
		Statement stm = null;
		ResultSet rs = null;
		String sql = "select * from menutbl";
		List<Menu> list = new ArrayList<Menu>();
		try {
			con = DBUtil.openConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(sql);
			while(rs.next()){
				Menu menu = new Menu();
				menu.setId(rs.getInt("id"));
				menu.setTypeId(rs.getInt("typeid"));
				menu.setMenuItemNo(rs.getString("menuItemNO"));
				menu.setName(rs.getString("name"));
				menu.setPrice(rs.getFloat("price"));
				menu.setStatus(rs.getString("status"));
				menu.setPic(rs.getString("pic"));
				menu.setRemark(rs.getString("remark"));
				list.add(menu);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			DBUtil.close(rs, stm, con);
		}
		
		return list;
	}

}
