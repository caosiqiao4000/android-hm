package com.amaker.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.amaker.dao.ITableDao;
import com.amaker.entity.CheckTable;
import com.amaker.util.DBUtil;

public class TableDaoImpl implements ITableDao {

	public List<CheckTable> findAllTable() {
		Connection con = null;
		Statement stm = null;
		ResultSet rs = null;
		String sql = "select * from tabletbl";
		List<CheckTable> list = new ArrayList<CheckTable>();
		try {
			con = DBUtil.openConnection();
			stm = con.createStatement();
			rs = stm.executeQuery(sql);
			while(rs.next()){
				CheckTable ct = new CheckTable();
				ct.setId(rs.getInt("id"));
				ct.setNum(rs.getInt("num"));
				ct.setFlag(rs.getString("flag"));
				ct.setDescription(rs.getString("description"));
				list.add(ct);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}

	public void updateStatus(int sourceTableId, int destTableId) throws Exception{
		// TODO Auto-generated method stub
		Connection con = null;
		CallableStatement call = null;
		
		try {
			con = DBUtil.openConnection();
			call = con.prepareCall("{call new_proc(?,?)}");
			call.setInt(1, sourceTableId);
			call.setInt(2, destTableId);
			call.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception("���ݿ����");
		}
	}

}
