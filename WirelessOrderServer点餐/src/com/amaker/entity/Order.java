package com.amaker.entity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Order {
	// 编号
	private int id;
	// 下单时间
	private Timestamp orderTime;
	// 下单用户
	private int userId;
	// 桌号
	private int tableId;
	// 人数
	private int personNum;
	// 是否结算
	private String isPay;
	// 备注
	private String remark;
	
	private List<OrderDetail> details = new ArrayList<OrderDetail>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getPersonNum() {
		return personNum;
	}
	public void setPersonNum(int personNum) {
		this.personNum = personNum;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getTableId() {
		return tableId;
	}
	public void setTableId(int tableId) {
		this.tableId = tableId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getIsPay() {
		return isPay;
	}
	public void setIsPay(String isPay) {
		this.isPay = isPay;
	}
	public Timestamp getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(Timestamp orderTime) {
		this.orderTime = orderTime;
	}
	public List<OrderDetail> getDetails() {
		return details;
	}
	public void setDetails(List<OrderDetail> details) {
		this.details = details;
	}
	
}
