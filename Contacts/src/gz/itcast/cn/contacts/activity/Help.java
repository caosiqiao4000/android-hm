package gz.itcast.cn.contacts.activity;

import java.util.ArrayList;

import gz.itcast.cn.contacts.R;
import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Help extends Activity implements OnClickListener {
	int[] imageIds = {R.drawable.guide01, R.drawable.guide02,R.drawable.guide03,R.drawable.guide04,R.drawable.guide05};
	ViewPager pager;
	LinearLayout dotLayout; // 圆点所处的Layout
	int lastPosition;       // 上一个页面的位置
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		
		ViewPager pager = (ViewPager) findViewById(R.id.pager);
		// 要显示的图片控件
		ArrayList<View> views = new ArrayList<View>();
		// 初始化图片
		for (int imageId : imageIds) {
			ImageView image = new ImageView(this);
			// 设置图片
			image.setBackgroundResource(imageId);
			views.add(image);
		}
		
		// 设置适配器
		pager.setAdapter(new HelpAdapter(views));
		// 设置监听器
		pager.setOnPageChangeListener(new HelpListener());
		this.pager = pager;
		
		// 圆点所在的布局
		LinearLayout dotLayout = (LinearLayout) findViewById(R.id.dot_layout);
		int childCount = dotLayout.getChildCount();
		for (int i = 0; i < childCount; i++) { // 遍历圆点
			ImageView image = (ImageView) dotLayout.getChildAt(i);
			image.setOnClickListener(this);
			image.setTag(i); // 绑定相应的索引
			if (i == 0) { // 默认第一个不可以点击
				image.setEnabled(false);
			}
		}
		this.dotLayout = dotLayout;
	}
	
	private class HelpAdapter extends PagerAdapter {
		ArrayList<View> views;
		
		public HelpAdapter(ArrayList<View> views) {
			this.views = views;
		}
		
		/**
		 * 销毁position对应的页面
		 */
		public void destroyItem(View view, int position, Object arg2) {
			ViewPager pager = (ViewPager) view;
			View image = views.get(position);
			pager.removeView(image); // 移除对应的View
		}
		
		/**
		 * 页面的总数
		 */
		public int getCount() {
			return views.size();
		}

		/**
		 * 初始化position对应的页面
		 */
		public Object instantiateItem(View view, int position) {
			ViewPager pager = (ViewPager) view;
			View image = views.get(position);
			pager.addView(image); // 添加对应的View
			return image;
		}

		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}
		public void restoreState(Parcelable arg0, ClassLoader arg1) {
		}
		public Parcelable saveState() {
			return null;
		}
		public void startUpdate(View arg0) {}
		public void finishUpdate(View arg0) {
		}
	}
	
	private class HelpListener implements OnPageChangeListener {
		/**
		 * 页面滚动状态改变
		 */
		public void onPageScrollStateChanged(int state) {
		}

		/**
		 * 正在滚动
		 */
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		/**
		 * 某个页面被选中
		 */
		public void onPageSelected(int position) {
			// 设置上一个页面的圆点 可以 点击
			dotLayout.getChildAt(lastPosition).setEnabled(true);
			
			// 设置当前的圆点  不可以 点击
			dotLayout.getChildAt(position).setEnabled(false);
			
			lastPosition = position; // 赋值
		}
	}

	public void onClick(View v) {
		// 得到ImageView绑定的索引
		int index = (Integer) v.getTag();
		pager.setCurrentItem(index); // 切换界面
	}
}
