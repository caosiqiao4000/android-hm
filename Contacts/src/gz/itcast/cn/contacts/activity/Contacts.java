package gz.itcast.cn.contacts.activity;

import gz.itcast.cn.contacts.R;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class Contacts extends ListActivity {
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts);
    }

	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.contacts, menu);
		return super.onCreateOptionsMenu(menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.menu_add: // 添加联系人
			break;
		case R.id.menu_delete: // 批量删除
			break;
		case R.id.menu_help: // 帮助
			Intent intent = new Intent(this, Help.class);
			startActivity(intent);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}