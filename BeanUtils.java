package gz.easyjf.color8.util;

import gz.easyjf.color8.model.pintu.Layer;
import gz.easyjf.color8.model.pintu.Sprite;
import gz.easyjf.color8.view.PintuView;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
/**
 * 对象属性拷贝
 * @author MJ
 */
@SuppressWarnings("unchecked")
public class BeanUtils {
	
	/**
	 * copy对象
	 * @param dest 目标对象 
	 * @param src 源对象
	 */
	public static void copy(Object dest, Object src) {
		Class clazz = src.getClass(); //得到对象的类型
		Field[] fields = clazz.getDeclaredFields(); //得到对象的所有属性
		List<Field> fieldList = new ArrayList<Field>();
		
		Class superClass = null;
		while((superClass = clazz.getSuperclass()) != null) {
			Field[] superFileds = superClass.getDeclaredFields();
			if(superFileds!=null) {//添加父类的属性
				for(Field f : superFileds) {
					fieldList.add(f);
				}
			}
			clazz = superClass;
		}
		
		if(fields!=null) {//添加自己的属性
			for(Field f : fields) {
				if(f.getType() == PintuView.class
						||f.getType() == Layer.class
						||f.getType() == ColorMatrix.class) {
					continue;
				}
				fieldList.add(f);
			}
		}
		
		for(Field field : fieldList) {
			invoke(dest, src, field);
		}
	}
	
	/**
	 * 回调getter
	 */
	private static Object invokeGetter(Field f, Object o) {
		String fieldName = f.getName();
		try {
			Method m =  o.getClass().getMethod("get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1), new Class[]{});
			Object ret = m.invoke(o, new Object[]{});
			if(ret instanceof Bitmap) {
				return Bitmap.createBitmap((Bitmap) ret);
			} else if(ret instanceof Paint) {
				return new Paint((Paint) ret);
			}
			return ret;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 回调setter
	 */
	private static void invokeSetter(Field field, Object object, Object value) {
		String fieldName = field.getName();
		String methodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
		try {
			Method m =  object.getClass().getMethod(methodName, field.getType());
			m.invoke(object, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 设值
	 * @param dest 目标对象
	 * @param src 源对象
	 * @param field 复制的属性
	 */
	private static void invoke(Object dest, Object src, Field field) {
		invokeSetter(field, dest, invokeGetter(field, src));
	}
	
	/**
	 * copy所有图层内容
	 * @param oldLayers
	 * @return
	 */
	public static List<Layer> copy(List<Layer> oldLayers) {
		List<Layer> newLayers = new ArrayList<Layer>(); 
		for(Layer oldLayer : oldLayers) {
			Layer newLayer = new Layer();
			for(Sprite oldSprite : oldLayer.getElements()) {
				Sprite newSprite = new Sprite();
				copy(newSprite, oldSprite);
				newLayer.add(newSprite);
			}
			newLayers.add(newLayer);
		}
		return newLayers;
	}
}
