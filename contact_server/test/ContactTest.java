import java.util.Date;

import org.springframework.test.jpa.AbstractJpaTests;

import cn.itcast.gz.contact.model.Contact;
import cn.itcast.gz.contact.service.IContactService;


public class ContactTest extends AbstractJpaTests {
	
	@Override
	protected String[] getConfigLocations() {
		return new String[]{"applicationContext.xml"};
	}
	
	private IContactService contactService;
	public void setContactService(IContactService contactService) {
		this.contactService = contactService;
	}


	public void testAdd() {
		for (int i = 0; i < 30; i++) {
			int index = i%9 + 1;  /// [1,9]
			
			Contact c = new Contact();
			c.setName("itcast"+i);
			c.setPhone("10086"+i);
			c.setBirthday(new Date());
			c.setEmail("10086"+i+"@qq.com");
			c.setSex(0);
			c.setPhoto("images/h00"+index+".png");
			contactService.add(c);
		}
		
		//this.setComplete(); // 同步到数据库
	}
	
	public void testFind() {
		System.out.println(contactService.find(2, 5));
	}
}
