package cn.itcast.gz.contact.model;

import java.util.List;

public class PageModel<T> {
	private int totalNum;
	private int totalPages;
	private List<T> data;
	private int pageSize;
	private int pageNo;
	public int getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	@Override
	public String toString() {
		return "{\"data\":" + data + ", \"pageNo\":" + pageNo + ", \"pageSize\":"
				+ pageSize + ", \"totalNum\":" + totalNum + ", \"totalPages\":"
				+ totalPages + "}";
	}
}
