package cn.itcast.gz.contact.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import cn.itcast.gz.contact.model.Contact;
@SuppressWarnings("unchecked")
@Repository
public class ContactDao implements IContactDao {
	@PersistenceContext
	private EntityManager entityManager;
	
	
	public Serializable add(Contact contact) {
		entityManager.persist(contact);
		return contact.getId();
	}

	public void delete(Serializable id) {
		entityManager.remove(id);
	}
	
	public List<Contact> find(int start, int size) {
		Query query = entityManager.createQuery("from Contact");
		query.setFirstResult(start);
		query.setMaxResults(size); // limit start,size
		return query.getResultList();
	}

	public void update(Contact contact) {
		entityManager.merge(contact);
	}

	public <T>T executeJpql(String jpql, Object[] params) {
		Query query = entityManager.createQuery(jpql);
		if (params != null) {
			int length = params.length;
			for (int i = 0; i < length; i++) {
				query.setParameter(i + 1, params[i]);
			}
		}
		List list = query.getResultList();
		if (list.size() == 1) {
			return (T) list.get(0);
		}
		return (T) list;
	}
	
}
