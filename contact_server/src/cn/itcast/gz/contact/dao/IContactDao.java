package cn.itcast.gz.contact.dao;

import java.io.Serializable;
import java.util.List;

import cn.itcast.gz.contact.model.Contact;

public interface IContactDao {
	public Serializable add(Contact contact);
	public void delete(Serializable id);
	public void update(Contact contact);
	public List<Contact> find(int start, int size);
	
	public <T>T executeJpql(String jpql, Object[] params);
}
