package cn.itcast.gz.contact.service;

import java.io.Serializable;
import java.util.List;

import cn.itcast.gz.contact.model.Contact;

public interface IContactService {
	public Serializable add(Contact contact);
	public void delete(Serializable id);
	public void update(Contact contact);
	public List<Contact> find(int pageNo, int pageSize);
	public int getNum();
}
