package cn.itcast.gz.contact.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.gz.contact.dao.IContactDao;
import cn.itcast.gz.contact.model.Contact;

@Service
public class ContactService implements IContactService {
	@Autowired
	private IContactDao contactDao;
	
	public Serializable add(Contact contact) {
		return contactDao.add(contact);
	}

	public void delete(Serializable id) {
		contactDao.delete(id);
	}
	
	public List<Contact> find(int pageNo, int pageSize) {
		int start = pageNo * pageSize;
		return contactDao.find(start, pageSize);
	}

	public void update(Contact contact) {
		contactDao.update(contact);
	}

	public int getNum() {
		Long num = contactDao.executeJpql(" select count(*) from Contact ", null);
		return num.intValue();
	}

}
