package cn.itcast.gz.contact.util;

import org.hibernate.cfg.ImprovedNamingStrategy;
import org.hibernate.util.StringHelper;

@SuppressWarnings("serial")
public class MyNamingStrategy extends ImprovedNamingStrategy {

	// 把类名转换成表名  Contact -->> t_contact
	public String classToTableName(String className) {
		return "T_" + addUnderscores(StringHelper.unqualify(className)).toUpperCase();
	}

	// 把属性名转换成表的字段名  userNameAbc-->> USER_NAME_ABC
	public String propertyToColumnName(String propertyName) {
		return addUnderscores(StringHelper.unqualify(propertyName)).toUpperCase();
	}
}
