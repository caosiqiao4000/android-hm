package cn.itcast.gz.contact.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import cn.itcast.gz.contact.model.Contact;
import cn.itcast.gz.contact.model.PageModel;
import cn.itcast.gz.contact.service.IContactService;

import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
@Controller
public class ContactAction extends ActionSupport {
	@Autowired
	private IContactService contactService;
	private Contact contact;
	public Contact getContact() {
		return contact;
	}
	
	private int pageNo;
	private int pageSize;
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 添加
	 */
	public void add() {
		contactService.add(contact);
	}
	
	public void find() {
		int num = contactService.getNum();
		int totalPages = (num + pageSize -1)/pageSize;
		if (pageNo < 0) {
			pageNo = 0;
		}
		
		if (pageNo > totalPages) {
			pageNo = totalPages;
		}
		List<Contact> contacts = contactService.find(pageNo, pageSize);
		PageModel<Contact> model = new PageModel<Contact>();
		model.setData(contacts);
		model.setPageNo(pageNo);
		model.setPageSize(pageSize);
		model.setTotalNum(num);
		model.setTotalPages(totalPages);
		
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("text/html; charset=UTF-8");
		
		try {
			response.getWriter().write(model.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
