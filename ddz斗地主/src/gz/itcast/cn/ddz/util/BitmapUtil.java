package gz.itcast.cn.ddz.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;

public class BitmapUtil {
	
	/**
	 * 生成指定宽度和高度的图片
	 */
	public static Bitmap getBitmap(Context context, int resId, int width, int height) {
		// 得到一张白纸
		Bitmap paper = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		// 画板 ，  将白纸贴到画板上面
		Canvas canvas = new Canvas(paper);
		// 得到要画的东西
		Drawable image = context.getResources().getDrawable(resId);
		image.setBounds(0, 0, width, height);
		image.draw(canvas);
		return paper;
	}
}
