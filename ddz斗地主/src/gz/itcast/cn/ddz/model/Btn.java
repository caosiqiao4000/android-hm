package gz.itcast.cn.ddz.model;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class Btn {
	public Bitmap icon; // 图片
	public int x, y; // 坐标
	public boolean show = true; // 是否要显示
	
	/**
	 * 画自己
	 */
	public void draw(Canvas canvas, Paint paint) {
		if(show) {
			canvas.drawBitmap(icon, x, y, paint);
		}
	}
	
	/**
	 * 判断自己是否被点击
	 * @param touchX 手指的触摸坐标
	 * @param touchY
	 */
	public boolean isTouch(float touchX, float touchY) {
		return     touchX > x 
				&& touchX < (x+icon.getWidth())  
				&& touchY > y 
				&& touchY < (y+icon.getHeight());
	}
}
