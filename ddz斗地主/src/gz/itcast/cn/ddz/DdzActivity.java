package gz.itcast.cn.ddz;

import gz.itcast.cn.ddz.mgr.SoundMgr;
import gz.itcast.cn.ddz.view.GameView;
import android.app.Activity;
import android.os.Bundle;

public class DdzActivity extends Activity {
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SoundMgr.initSounds(this); // 初始化音效
        setContentView(new GameView(this));
    }
    
}