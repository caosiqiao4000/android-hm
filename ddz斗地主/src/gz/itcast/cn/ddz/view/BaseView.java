package gz.itcast.cn.ddz.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

public abstract class BaseView extends SurfaceView implements Callback, Runnable {
	// Surface的大管家
	SurfaceHolder holder;
	// 绘图线程
	Thread thread;
	// 是否正在绘图
	boolean running;
	
	// 百叶窗中正方形的边长
	private static final int CUBE_BEGIN = 30;
	private static final int CUBE_END = 0; // 百叶窗画完后, 正方形的边长
	private static final int MINUS_STEP = 2; // 正方形边长减小的幅度
	private int cube = CUBE_BEGIN;
	int cubeRow, cubeCol;
	
	// 屏幕尺寸
	protected int screenWidth;
	protected int screenHeight;
	protected Paint paint;
	
	// 文字大小
	protected static final int TEXT_SIZE = 25;
	
	public BaseView(Context context) {
		super(context);
		// 得到SurfaceHolder
		holder = getHolder();
		// 添加Surface生命周期的监听器
		holder.addCallback(this);
		
		Activity activity = (Activity)context;
		screenWidth = activity.getWindowManager().getDefaultDisplay().getWidth();
		screenHeight = activity.getWindowManager().getDefaultDisplay().getHeight();
		
		cubeRow = screenHeight/cube;
		cubeCol = screenWidth/cube;
		
		paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setTextSize(TEXT_SIZE);
		paint.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/num.ttf"));
	}

	/**
	 * 当Surface创建的时候调用
	 */
	public void surfaceCreated(SurfaceHolder holder) {
		// 开启绘图线程
		System.out.println("surfaceCreated");
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	/**
	 * 当Surface的大小改变的时候调用
	 */
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		System.out.println("surfaceChanged");
	}

	/**
	 * 当Surface销毁的时候调用
	 */
	public void surfaceDestroyed(SurfaceHolder holder) {
		// 销毁绘图线程
		System.out.println("surfaceDestroyed");
		
		running = false;
		if(thread!=null && thread.isAlive()) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	int y = 100;
	/**
	 * 绘图
	 */
	public void run() {
		System.out.println("run");
		// 画布
		Canvas canvas = null;
		// 将成员变量转为局部变量
		SurfaceHolder holder = this.holder;
		
 		while (running) {
			try {
				Thread.sleep(100);
				
				synchronized (holder) {
					canvas = holder.lockCanvas();
					// 在后台线程绘图
					render(canvas);
					
					// 画百叶窗
					drawWindow(canvas);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				// 回到主线程渲染
				if (canvas != null) {
					holder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}
	
	private void drawWindow(Canvas canvas) {
		if (cube<= CUBE_END) return;
		for (int row = 0; row < cubeRow; row++) {
			for (int col = 0; col < cubeCol; col++) {
				// 固定左上角
				int left = col*CUBE_BEGIN;
				int top = row*CUBE_BEGIN;
				canvas.drawRect(left, top, left+cube, top+cube, paint);
			}
		}
		cube -= MINUS_STEP;
	}

	/**
	 * 真正的绘图逻辑
	 */
	public abstract void render(Canvas canvas);
}
