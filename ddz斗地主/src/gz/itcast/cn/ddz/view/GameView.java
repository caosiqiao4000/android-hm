package gz.itcast.cn.ddz.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import gz.itcast.cn.ddz.R;
import gz.itcast.cn.ddz.mgr.SoundMgr;
import gz.itcast.cn.ddz.model.Btn;
import gz.itcast.cn.ddz.model.Card;
import gz.itcast.cn.ddz.model.Player;
import gz.itcast.cn.ddz.util.BitmapUtil;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.widget.Toast;

public class GameView extends BaseView implements Comparator<Card>{
	// 离屏幕边缘的间距
	private static final int MARGIN = 10;
	Bitmap bg; // 背景图片
	Bitmap back; // 卡片背面
	// 卡片尺寸
	int cardWidth;
	int cardHeight;
	int miniCardWidth;
	int miniCardHeight;
	int cardMargin;
	
	// 玩家
	ArrayList<Player> players;
	// 自己
	Player me;
	// 所有的牌
	ArrayList<Card> cards;
	// 底牌
	ArrayList<Card> bottomCards;
	
	// 按钮
	Btn cpBtn; // 出牌
	Btn bcBtn; // 不出

	public GameView(Context context) {
		super(context);
		//bg = BitmapFactory.decodeResource(getResources(), R.drawable.bg);
		//bg = Bitmap.createScaledBitmap(bg, width, height, true);
		bg = BitmapUtil.getBitmap(context, R.drawable.bg, screenWidth, screenHeight);
		
		initCards();
		initPlayers();
		initBtns();
		beginNewGame();
	}
	
	/**
	 * 开启新游戏
	 */
	private void beginNewGame() {
		cleanCards();
		distributeCards();
	}
	
	/**
	 * 初始化玩家信息
	 */
	private void initPlayers() {
		players = new ArrayList<Player>();
		
		// 自己
		Bitmap meIcon = BitmapFactory.decodeResource(getResources(), R.drawable.me);
		Player me = new Player();
		me.computer = false;
		me.lord = false;
		me.name = "West Door1";
		me.icon = meIcon;
		me.x = MARGIN;
		// 屏幕高度 - 卡片高度 - 头像高度 - 文字高度
		me.y = screenHeight - cardHeight - meIcon.getHeight() - TEXT_SIZE - MARGIN;
		this.me = me;
		
		// 左边的玩家
		Bitmap leftIcon = BitmapFactory.decodeResource(getResources(), R.drawable.left);
		Player left = new Player();
		left.computer = true;
		left.lord = true;
		left.name = "West Door2";
		left.icon = leftIcon;
		left.x = MARGIN;
		left.y = MARGIN;
		
		// 右边的玩家
		Bitmap rightIcon = BitmapFactory.decodeResource(getResources(), R.drawable.right);
		Player right = new Player();
		right.computer = true;
		right.lord = false;
		right.name = "West Door3";
		right.icon = rightIcon;
		// 屏幕宽度 - 头像宽度 - 间距
		right.x = screenWidth - MARGIN - rightIcon.getWidth();
		right.y = MARGIN; 
		
		// 添加玩家  
		players.add(left.next = me);  
		players.add(right.next = left);  
		players.add(me.next = right);
	}
	
	/**
	 * 洗牌
	 */
	private void cleanCards() {
		ArrayList<Card> cards = this.cards;
		// Math.random() -- (int)((0, 1) * 10) = [0, 9]
		for (int i = 0; i < 100; i++) {
			int size = cards.size();
			int aIndex = (int) (Math.random() * size); // [0, 53]
			int bIndex = (int) (Math.random() * size);
			if (aIndex == bIndex) continue;
			
			Card aCard = cards.get(aIndex);
			Card bCard = cards.get(bIndex);
			
			cards.set(aIndex, bCard);
			cards.set(bIndex, aCard); // 交换卡片
		}
	}
	
	/**
	 * 发牌
	 */
	private void distributeCards() {
		ArrayList<Player> players = this.players;
		for (Player player : players) {
			player.clearAll();
		}
		
		ArrayList<Card> cards = this.cards;
		int size = players.size();
		int cardCount = cards.size() - 3;
		
		for (int i = 0; i < cardCount/size; i++) {
			for (int j = 0; j < size; j++) {
				players.get(j).handCards.add(cards.get(i*size + j));
			}
		}
		
		ArrayList<Card> bottomCards = this.bottomCards;
		bottomCards.clear();
		// 底牌
		bottomCards.add(cards.get(51));
		bottomCards.add(cards.get(52));
		bottomCards.add(cards.get(53));
		Collections.sort(bottomCards, this);
		
		// 对玩家的牌排序
		for (int j = 0; j < size; j++) {
			Player player = players.get(j);
			ArrayList<Card> handCards = player.handCards;
			
			if (player.lord) { // 如果是地主， 就加底牌到手牌中
				handCards.addAll(bottomCards);
			}
			Collections.sort(handCards, this);
		}
	}
	
	/**
	 * 比较卡片的大小
	 */
	public int compare(Card card1, Card card2) {
		int step = card1.value - card2.value;
		return (step==0)?(card1.color - card2.color):step;
	}
	
	/**
	 * 初始化牌
	 */
	private void initCards() {
		cards = new ArrayList<Card>();
		bottomCards = new ArrayList<Card>();
		
		// 固定卡片的宽度和高度
		cardWidth = screenWidth / 13;
		cardHeight = screenHeight / 5;
		cardMargin = cardWidth/2;
		
		miniCardWidth = cardWidth*2/3;
		miniCardHeight = cardHeight*2/3;
		
		// 固定整副牌的宽度和高度
		Bitmap poker = BitmapUtil.getBitmap(getContext(), R.drawable.poker, cardWidth*13, cardHeight*5);
		
		Bitmap miniPoker = BitmapUtil.getBitmap(getContext(), R.drawable.poker_mini, miniCardWidth*13, miniCardHeight*5);
		 // 得到卡片背面
		back = Bitmap.createBitmap(miniPoker, 2*miniCardWidth, 4*miniCardHeight, miniCardWidth, miniCardHeight);
		
		for (int row=0; row < 5; row ++) {  // 遍历每一行
			for (int col=0; col < 13; col ++) {  // 遍历每一列
				if (row == 4 && col>1) break; // 不要的卡片
				
				Card card = new Card();
				card.bigIcon = Bitmap.createBitmap(poker, 
						cardWidth*col, cardHeight*row, cardWidth, cardHeight);
				card.miniIcon = Bitmap.createBitmap(miniPoker, 
						miniCardWidth*col, miniCardHeight*row, miniCardWidth, miniCardHeight);
				card.color = row;  // 花色等于行号
				card.value = col + 1; // 默认等于 列+1
				
				// 前两列特殊处理
				if (col < 2) {
					card.value = card.value + 13; // 补回13
					if (row == 4) {
						if (col == 0) {
							card.value = 17; // 大王
						} else {
							card.value = 16; // 小王
						}
					}
				}
				
				cards.add(card);
			}
		}
		
		System.out.println(cards.size());
	}
	
	
	
	/**
	 * 初始化按钮
	 */
	private void initBtns() {
		// 不出
		Bitmap bcIcon = BitmapFactory.decodeResource(getResources(), R.drawable.bc);
		Btn bcBtn = new Btn();
		bcBtn.icon = bcIcon;
		bcBtn.y = screenHeight - cardHeight - bcIcon.getHeight() - MARGIN*3;
		bcBtn.x = screenWidth - bcIcon.getWidth() - MARGIN;
		this.bcBtn = bcBtn;
		
		// 出牌
		Bitmap cpIcon = BitmapFactory.decodeResource(getResources(), R.drawable.cp);
		Btn cpBtn = new Btn();
		cpBtn.icon = cpIcon;
		cpBtn.y = bcBtn.y;
		cpBtn.x = bcBtn.x - cpIcon.getWidth() - MARGIN*2;
		this.cpBtn = cpBtn;
	}
	
	@Override
	public void render(Canvas canvas) {
		Paint paint = this.paint;
		
		canvas.drawBitmap(bg, 0, 0, paint);
		
		// 画玩家的信息
		ArrayList<Player> players = this.players;
		synchronized (players) {
			for (Player player : players) {
				// 画玩家的头像
				canvas.drawBitmap(player.icon, player.x, player.y, paint);
				// 画文字
				String text = player.name+"("+player.score+")"; // 名字、积分
				int textX = player.x;
				// 右边的玩家
				if (me.next == player) {
					Rect bounds = new Rect();
					// 测量文字尺寸
					paint.getTextBounds(text, 0, text.length(), bounds);
					int textWidth =  bounds.right - bounds.left;
					// 屏幕宽度 - 间距 - 文字宽度
					textX = screenWidth - MARGIN - textWidth;
				}
				canvas.drawText(text,
						textX, player.y + player.icon.getHeight() + TEXT_SIZE, paint);
				
				// 画电脑的手牌(背面)
				if (player != me) {
					// 默认是左边的玩家
					int handCardSize = player.handCards.size();
					int iconHeight = player.icon.getHeight();
					int backX = MARGIN;
					if (me.next == player) { // 右边的玩家
						backX = screenWidth - MARGIN - miniCardWidth;
					}
					int backY = player.y + iconHeight + TEXT_SIZE + MARGIN;
					canvas.drawBitmap(back, backX, backY, paint);
					// 数目
					int sizeX = backX;
					int sizeY = backY + miniCardHeight + TEXT_SIZE + MARGIN;
					canvas.drawText(handCardSize+"", sizeX, sizeY, paint);
					
					// 画打出去的牌
					ArrayList<Card> sentCards = player.sentCards;
					int sentSize = sentCards.size();
					int sentStartX = backX + miniCardWidth + MARGIN;
					if (me.next == player) { // 右边的玩家
						// 卡片宽度
						int sentCardWidth = (sentSize - 1)*cardMargin + miniCardWidth;
						sentStartX = backX - MARGIN - sentCardWidth;
					}
					int sentStartY = backY;
					for (int i = 0; i < sentSize; i++) {
						canvas.drawBitmap(sentCards.get(i).miniIcon, 
								sentStartX + i * cardMargin, sentStartY, paint);
					}
				}	
			}
		}
		
		// 画自己的手牌
		ArrayList<Card> handCards = me.handCards;
		synchronized (handCards) {
			int handCardSize = handCards.size();
			int handCardWidth = (handCardSize - 1)* cardMargin + cardWidth; // 手牌宽度
			int handCardStartX = (screenWidth - handCardWidth)/2;
			for (int i = 0; i < handCardSize; i++) {
				Card card = handCards.get(i);
				Bitmap cardMap = card.bigIcon;
				int y = screenHeight - cardHeight;
				// 如果被提起了, 就减小y
				if (me.willCards.contains(card)) {
					y -= MARGIN * 2;
				}
				canvas.drawBitmap(cardMap, handCardStartX + i*cardMargin, y, paint);
			}
		}
		
		// 画自己打出去的牌
		ArrayList<Card> sentCards = me.sentCards;
		int sentCardSize = sentCards.size();
		int sentCardWidth = (sentCardSize-1)* cardMargin + cardWidth; // 打出去的牌宽度
		int sentCardStartX = (screenWidth - sentCardWidth)/2;
		int sentCardY = screenHeight - cardHeight*2 - MARGIN;
		for (int i = 0; i < sentCardSize; i++) {
			canvas.drawBitmap(sentCards.get(i).bigIcon, 
					sentCardStartX + i*cardMargin, sentCardY, paint);
		}
		
		// 画底牌
		ArrayList<Card> bottomCards = this.bottomCards;
		int size = bottomCards.size();
		// 开始X坐标
		int bottomStartX = (screenWidth - size*miniCardWidth)/2;
		for (int i = 0; i < size; i++) {
			canvas.drawBitmap(bottomCards.get(i).miniIcon, bottomStartX + i*miniCardWidth, MARGIN, paint);
		}
		
		// 画按钮
		bcBtn.draw(canvas, paint);
		cpBtn.draw(canvas, paint);
	}
	
	int lastIndex = -1;
	
	/**
	 * 监听触摸事件
	 */
	public boolean onTouchEvent(MotionEvent event) {
		// MotionEvent.ACTION_DOWN  手指按下
		// MotionEvent.ACTION_MOVE  手指开始移动
		// MotionEvent.ACTION_UP    手指抬起
		SoundMgr.play(Card.CLICK);
		
		if (event.getAction() == MotionEvent.ACTION_UP) {
			lastIndex = -1; // 清空索引
		}
		
		// 得到触摸的位置
		float x = event.getX();
		float y = event.getY();
		
		ArrayList<Card> handCards = me.handCards;
		int size = handCards.size();
		int handCardWidth = (size - 1)* cardMargin + cardWidth; // 手牌宽度
		int handCardStartX = (screenWidth - handCardWidth)/2;
		
		if (x>handCardStartX && x< (handCardStartX +handCardWidth) 
				&& y>( screenHeight - cardHeight)) {
			// 计算出卡片的下标
			int index = (int) ((x - handCardStartX)/cardMargin);
			// 如果越界
			if (index >= size) {
				index = size - 1;
			}
			
			if (lastIndex == index) { // 如果跟上次一样, 就不需要执行下面的代码
				return true;
			}
			
			ArrayList<Card> willCards = me.willCards;
			Card card = handCards.get(index);
			if (willCards.contains(card)) { // 如果已经被提起来
				willCards.remove(card);
			} else {
				willCards.add(card);
			}
			
			lastIndex = index; // 记录这次的index
		} else if (bcBtn.isTouch(x, y)) { // 点击了不出
			me.willCards.clear();
			sendCards(me); // 不出相当于出空牌
		} else if (cpBtn.isTouch(x, y)) { // 点击了出牌
			if (me.willCards.isEmpty()) {
				Toast.makeText(getContext(), "请选牌", Toast.LENGTH_SHORT).show();
			} else {
				sendCards(me);
			}
		}
		return true;
	}
	
	/**
	 * 出牌
	 */
	private void sendCards(Player player) {
		ArrayList<Card> willCards = player.willCards;
		ArrayList<Card> handCards = player.handCards;
		ArrayList<Card> sentCards = player.sentCards;
		sentCards.clear(); // 清理上一次打的牌
		
		// 清理手牌
		handCards.removeAll(willCards);
		// 添加打出去的牌
		sentCards.addAll(willCards);
		willCards.clear();
		
		if (!sentCards.isEmpty()) {
			Card card = sentCards.get(0);
			SoundMgr.play(card.value);
			SoundMgr.play(Card.SEND_CARD);
		}
		
		
		// 判断输赢
		if (handCards.isEmpty()) { // 有人打完牌了
			if ((player.lord && me.lord) || (!player.lord && !me.lord)) { // 地主赢了
				new AlertDialog.Builder(getContext())
				.setPositiveButton("再来一盘", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						beginNewGame();
					}
				})
				.setNegativeButton("取消", null)
				.setTitle("竟然赢了")
				.setMessage("这么犀利！").create().show();
			} else {
				new AlertDialog.Builder(getContext())
				.setPositiveButton("再来一盘",  new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						beginNewGame();
					}
				})
				.setNegativeButton("取消", null)
				.setTitle("恭喜")
				.setMessage("这么杯具！").create().show();
			}
			return;
		}
		
		
		Player next = player.next; // 得到自己的下家
		if (next.computer) {// 如果下家是电脑, 就强迫出牌
			bcBtn.show = false; // 隐藏按钮
			cpBtn.show = false;

			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			for (int i = 0; i < 5; i++) {
				if (i >= next.handCards.size()) {
					break;
				}
				next.willCards.add(next.handCards.get(i)); // 取出第一张
			}
			sendCards(next);
		} else { // 轮到自己出牌, 显示按钮
			bcBtn.show = true;
			cpBtn.show = true;
		}
	}
}
