package gz.itcast.cn.ddz.mgr;

import java.util.HashMap;

import gz.itcast.cn.ddz.R;
import gz.itcast.cn.ddz.model.Card;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundMgr {
	private static SoundPool pool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
	private static HashMap<Integer, Integer> soundMap;
	private static AudioManager mgr;
	
	public static void play(Context context, int value, int rrr) {
		// int maxStreams :　能同时播放的最大音效数
		// int streamType : 音效的类型
		// int srcQuality :　采样转化率,　传0即可, API上指出这个参数暂时没什么效果
		// 创建音效池
		// int resId : 音效的资源id
		// int priority :　传1即可, API上指出这个参数暂时没什么效果
		// 加载音效文件
		// 此方法是采用异步加载
		int soundID = pool.load(context, R.raw.f_buyao1, 1);
		
		// int soundID :　用来播放是音效id
		// int leftVolume,rightVolume : 左声道右声道的音量
		// int priority :　播放的优先级, 数值越大优先级越高
		// int loop : -1代表无限循环, 其他情况 代表播放(loop+1)次
		// float rate : 播放速率, 0.5 - 2.0
		AudioManager mgr = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		int volume = mgr.getStreamVolume(AudioManager.STREAM_MUSIC); // 得到Music类型的音量
		pool.play(soundID, volume, volume, 1, 0, 1);
		
		System.out.println("play--------------------");
	}
	
	/**
	 * 通过卡片的Value来播放音效
	 * @param value
	 */
	public static void play(int value) {
		int volume = mgr.getStreamVolume(AudioManager.STREAM_MUSIC); // 得到Music类型的音量
		pool.play(soundMap.get(value), volume, volume, 1, 0, 1);
	}
	
	/**
	 * 初始化音效
	 */
	public static void initSounds(Context context) {
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		map.put(3, pool.load(context, R.raw.f_3, 1));
		map.put(4, pool.load(context, R.raw.f_4, 1));
		map.put(5, pool.load(context, R.raw.f_5, 1));
		map.put(6, pool.load(context, R.raw.f_6, 1));
		map.put(7, pool.load(context, R.raw.f_7, 1));
		map.put(8, pool.load(context, R.raw.f_8, 1));
		map.put(9, pool.load(context, R.raw.f_9, 1));
		map.put(10, pool.load(context, R.raw.f_10, 1));
		map.put(11, pool.load(context, R.raw.f_11, 1));
		map.put(12, pool.load(context, R.raw.f_12, 1));
		map.put(13, pool.load(context, R.raw.f_13, 1));
		map.put(14, pool.load(context, R.raw.f_1, 1)); // A
		map.put(15, pool.load(context, R.raw.f_2, 1));
		map.put(16, pool.load(context, R.raw.f_14, 1)); // 小王
		map.put(17, pool.load(context, R.raw.f_15, 1));
		
		map.put(Card.SEND_CARD, pool.load(context, R.raw.sendcard, 1)); // 打牌的声音
		map.put(Card.CLICK, pool.load(context, R.raw.click, 1)); // 点击屏幕的声音
		
		soundMap = map;
		
		mgr = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
	}
}
