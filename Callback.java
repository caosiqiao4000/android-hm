package gz.easyjf.color8.util;

/**
 * 回调接口
 * @author MJ
 *
 */
public interface Callback<P,R> {
	R execute(P param) throws Exception;
}
