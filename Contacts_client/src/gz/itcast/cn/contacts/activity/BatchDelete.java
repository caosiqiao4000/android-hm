package gz.itcast.cn.contacts.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import gz.itcast.cn.contacts.R;
import gz.itcast.cn.contacts.adapter.MyAdapter;
import gz.itcast.cn.contacts.adapter.MyAdapter.ItemModel;
import gz.itcast.cn.contacts.adapter.MyAdapter.RenderListener;
import gz.itcast.cn.contacts.mgr.ContactMgr;
import gz.itcast.cn.contacts.util.Constants;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
/**
 * 批量删除联系人
 * @author Administrator
 *
 */
@SuppressWarnings("unchecked")
public class BatchDelete extends ListActivity implements RenderListener, OnCheckedChangeListener, OnClickListener {
	ArrayList<Object> selectIds; // 要删除的id
	// 按钮
	Button btnDelete, btnCheck;
	ArrayList<HashMap<String, Object>> data; //联系人列表数据
	MyAdapter adapter; // 适配器
	int originSize; // 最初的联系人个数
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.batch_delete);
		
		// 设置标题
		TextView titleView = (TextView) findViewById(R.id.title);
		titleView.setText(R.string.batch_delete);
		
		Bundle bundle = getIntent().getExtras();
		// 得到传递过来的联系人数据
		ArrayList<HashMap<String, Object>> data = (ArrayList<HashMap<String, Object>>) bundle.get(Contacts.CONTACTS); // 拿到联系人列表数据
		originSize = data.size(); // 得到一开始的个数
		// 初始化适配器
		MyAdapter adapter = new MyAdapter(this, data, 
				R.layout.batch_delete_item, 
				new String[]{Constants.PHOTO, Constants.NAME, Constants.PHONE, Constants.ADDRESS}, 
        		new int[]{R.id.photo, R.id.name, R.id.phone, R.id.address,  R.id.check}, 
        		this);
        setListAdapter(adapter);
        this.adapter = adapter;
        this.data = data;
        
        selectIds = new ArrayList<Object>();
        
        // 按钮
        btnDelete = (Button) findViewById(R.id.btn_delete);
        btnDelete.setEnabled(false);
        btnDelete.setOnClickListener(this);
        
        btnCheck = (Button) findViewById(R.id.btn_all_no);
        btnCheck.setOnClickListener(this);
	}

	/**
	 * RenderListener
	 */
	public void render(ItemModel model) {
		CheckBox checkBox = model.getChild(R.id.check);
		// 绑定联系人的id
		Object contactId = model.getMapValue(Constants.ID);
		checkBox.setTag(contactId);
		
		// 检查之前是否被勾选
		checkBox.setChecked(selectIds.contains(contactId));
		
		// 在设置状态之后再绑定监听器
		checkBox.setOnCheckedChangeListener(this);
	}

	/**
	 * OnCheckedChangeListener
	 */
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		ArrayList<Object> selectIds = this.selectIds;
		Object contactId = buttonView.getTag(); // 得到checkbox绑定的联系人id
		if (isChecked) {
			if (!selectIds.contains(contactId)) { // 如果不包含这个id， 才有必要添加
				selectIds.add(contactId);
			}
		} else {
			selectIds.remove(contactId);
		}
		
		setDeleteBtnText(); //改变按钮的文字
		
		System.out.println(selectIds);
	}
	
	/**
	 *  改变按钮的文字
	 */
	private void setDeleteBtnText() {
		if (selectIds.isEmpty()) { // 没有可删除的数据
			btnDelete.setText(R.string.delete);
			btnDelete.setEnabled(false);
		} else {
			btnDelete.setText(getString(R.string.delete)+"("+selectIds.size()+")");
			btnDelete.setEnabled(true);
		}
	}
	
	/**
	 * 监听按钮点击
	 */
	public void onClick(View v) {
		ArrayList<Object> selectIds = this.selectIds;
		
		int id = v.getId();
		switch (id) {
		case R.id.btn_all_no: // 全选
			
			// 在这里只操纵集合
			if (selectIds.isEmpty()) { // 如果一个都还没有选,  那就要"全选"
				for (HashMap<String, Object> map : data) { // 加入所有的联系人id
					selectIds.add(map.get(Constants.ID));
				}
			} else if (selectIds.size() == data.size()){ // 全部选择了,  那就要"全不选"
				selectIds.clear(); // 清空集合
			} else { // 勾选了一部分, 那就要"全选"
				selectIds.clear(); // 清空集合
				for (HashMap<String, Object> map : data) { // 加入所有的联系人id
					selectIds.add(map.get(Constants.ID));
				}
			}
			
			adapter.notifyDataSetChanged(); // 通知数据改变, 重新渲染。Adapter就会重新调用getView
			
			setDeleteBtnText(); //改变按钮的文字
			break;
			
		case R.id.btn_delete: // 删除
			int ret = ContactMgr.get(this).deleteAll(selectIds);
			if (ret > 0) { // 删除成功
				Toast.makeText(this, "成功删除"+ret+"条", Toast.LENGTH_SHORT).show();
				
				// 利用迭代器删除内容(比较安全)
				for (Iterator iterator = data.iterator(); iterator.hasNext();) {
					// map数据
					HashMap<String, Object> map = (HashMap<String, Object>) iterator.next();
					// 得到对应的联系人id
					Object contactId = map.get(Constants.ID);
					// 如果是要删除的, 就从data中清除这个map
					if (selectIds.contains(contactId)) {
						iterator.remove();
					}
				}
				
				selectIds.clear(); // 清除上次删除的id
				
				adapter.notifyDataSetChanged(); // 重新渲染
				
				setDeleteBtnText(); // 改变按钮文字
			} else {
				Toast.makeText(this, R.string.delete_failure, Toast.LENGTH_SHORT).show();
			}
			break;	
		}
	}

	/**
	 * 监听item点击
	 */
	protected void onListItemClick(ListView l, View v, int position, long id) {
		CheckBox checkBox = (CheckBox) v.findViewById(R.id.check);
		checkBox.setChecked(!checkBox.isChecked()); // 取反
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// 监听返回键
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			ArrayList<HashMap<String, Object>> data = this.data;
			if (data.size() < originSize) { // 说明删除了一些数据
				Intent intent = new Intent();
				intent.putExtra(Contacts.CONTACTS, data);
				setResult(Contacts.CODE_REFRESH, intent); // 通知Contacts要刷新数据
			}
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
