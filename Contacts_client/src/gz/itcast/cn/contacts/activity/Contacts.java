package gz.itcast.cn.contacts.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;

import gz.itcast.cn.contacts.R;
import gz.itcast.cn.contacts.adapter.MyAdapter;
import gz.itcast.cn.contacts.adapter.MyAdapter.ItemModel;
import gz.itcast.cn.contacts.adapter.MyAdapter.RenderListener;
import gz.itcast.cn.contacts.mgr.ContactMgr;
import gz.itcast.cn.contacts.util.Callback;
import gz.itcast.cn.contacts.util.Constants;
import gz.itcast.cn.contacts.util.DialogUtil;
import gz.itcast.cn.contacts.util.HttpUtil;
import gz.itcast.cn.contacts.util.PinyinUtil;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
/**
 * 联系人列表
 * @author Administrator
 *
 */
@SuppressWarnings("unchecked")
public class Contacts extends ListActivity implements OnItemLongClickListener, 
OnItemClickListener, RenderListener, Comparator<HashMap<String, Object>>, Callback<JSONObject> {
	// 传递联系人数据的key
	public static final String CONTACT = "contact";   // 传递单个联系人
	public static final String CONTACTS = "contacts"; // 传递联系人集合
	
	// 跳到其他界面的请求码
	public static final int CODE_ADD_CONTACT = 2; // 跳到"添加联系人"
	public static final int CODE_BATCH_DELETE = 4; // 跳到"批量删除"
	
	public static final String ADD = "add"; //是否为添加的标识
	
	// 从其他界面回来的结果码
	public static final int CODE_REFRESH = 2; // 表示要刷新
	
	// 操作对话框的常量
	private static final int OPTION_CALL = 0; // 打电话
	private static final int OPTION_SEND = 1; // 发短信
	private static final int OPTION_EDIT = 2; // 编辑
	private static final int OPTION_DELETE = 3; // 删除
	
	// 当前被点击的map
	HashMap<String, Object> currentMap;
	int currentPosition;
	// 联系人数据
	ArrayList<HashMap<String, Object>> data;
	// 操作对话框
	Dialog optionDialog;
	// 搜索框
	EditText searchEt;
	// 适配器
	MyAdapter adapter;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts);
        // 搜索框
        EditText searchEt = (EditText) findViewById(R.id.search);
        // 设置文字监听器
        searchEt.addTextChangedListener(new SearchListner());
        this.searchEt = searchEt;
        
        // 加载数据
        loadData(null);
        
        // 设置长按监听器
        getListView().setOnItemLongClickListener(this);
    }
    
    /**
     * 加载数据
     */
    private void loadData(String condidation) {
    	
    	HashMap<String, Object> form = new HashMap<String, Object>();
    	form.put("pageNo", 1);
    	form.put("pageSize", 5);
    	
    	HttpUtil.post(this, "contact_find", form, this);
    }
    
    /**
     * Callback
     */
	public void execute(JSONObject t) throws Exception{
		int pageNo = t.getInt("pageNo");
		int totalNum = t.getInt("totalNum");
		int totalPages = t.getInt("totalPages");
		
		
		ArrayList<HashMap<String, Object>> data = new ArrayList<HashMap<String,Object>>();
		JSONArray array = t.getJSONArray("data"); // 一个JSONArray对应一个ArrayList
		
		int length = array.length();
		for (int i = 0; i<length; i++) {
			// 一个JSONObject对应一个HashMap
			JSONObject json = array.getJSONObject(i);
			HashMap<String, Object> map = new HashMap<String, Object>();
			
			// 属性拷贝
			for (Iterator iterator = json.keys(); iterator.hasNext();) {
				String key = (String) iterator.next();
				map.put(key, json.get(key));
			}
			
			data.add(map);
		}
		
		
		// 初始化适配器
    	MyAdapter adapter = new MyAdapter(this, data, R.layout.contacts_item, 
        		new String[]{Constants.NAME, Constants.PHONE, Constants.ADDRESS}, 
        		new int[]{R.id.name, R.id.phone, R.id.address}, this);
        setListAdapter(adapter);
        adapter.listView = getListView(); // 得到ListView
        this.adapter = adapter;
        
        // 设置搜索框提示文字
        searchEt.setHint(data.size()+"个联系人");
        
        this.data = data;
	}
    
    /**
     * 创建底部菜单
     */
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.contacts, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	/**
	 * 监听底部菜单点击
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = null;
		switch(item.getItemId()) {
		case R.id.menu_add: // 添加联系人
			intent = new Intent(this, AddContact.class);
			startActivityForResult(intent, CODE_ADD_CONTACT);
			break;
		case R.id.menu_delete: // 批量删除
			ArrayList<HashMap<String, Object>> data = this.data;
			if (data == null || data.isEmpty()) { // 没有可删除的数据
				Toast.makeText(this, R.string.no_data_delete, Toast.LENGTH_SHORT).show();
				return super.onOptionsItemSelected(item); // 直接返回
			}
			
			intent = new Intent(this, BatchDelete.class);
			intent.putExtra(CONTACTS, data); // 传递联系人数据
			startActivityForResult(intent, CODE_BATCH_DELETE);
			break;
		case R.id.menu_help: // 帮助
			// 初始化数据
			String[] provinces = getResources().getStringArray(R.array.provinces);
			ContactMgr mgr = ContactMgr.get(this);
			int length = provinces.length;
			for(int i=0; i<length; i++) {
				String p = provinces[i];
				ContentValues values = new ContentValues();
				values.put(Constants.NAME, p);
				values.put(Constants.PHONE, 10086+i);
				values.put(Constants.ADDRESS, p);
				values.put(Constants.PHOTO, "/mnt/sdcard/contacts/images/1334198672326.jpg");
				mgr.add(values);
			}
			
			intent = new Intent(this, Help.class);
			startActivity(intent);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * 从其他Activity返回
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (resultCode == CODE_REFRESH) { // 说明要刷新
			switch (requestCode) {
			case CODE_ADD_CONTACT:// 添加成功
				Bundle bundle = intent.getExtras();
				// 得到传回来的联系人数据
				ContentValues values = (ContentValues) bundle.get(CONTACT);
				// 将ContentValues转换为Set
				Set<Entry<String, Object>> set = (Set<Entry<String, Object>>) values.valueSet();
				
				HashMap<String, Object> map = new HashMap<String, Object>();
				for (Entry<String, Object> entry : set) { // 将values里面的数据全部导入到map中
					map.put(entry.getKey(), entry.getValue());
				}
				
				if (bundle.get(ADD) == null) { // 更新成功
					this.data.set(currentPosition, map); // 替换以前位置的map数据
				} else {
					this.data.add(map); // 添加返回的新数据
				}
				
				Collections.sort(this.data, this); // 排序
				adapter.notifyDataSetChanged(); // 重新渲染界面
				break;
			case CODE_BATCH_DELETE:
				// 从删除联系人界面返回的剩余数据
				ArrayList<HashMap<String, Object>> data = (ArrayList<HashMap<String, Object>>) intent.getExtras().get(CONTACTS);
				
				searchEt.setHint(data.size()+"个联系人");
				this.data.clear(); //  清除以前的数据
				this.data.addAll(data); // 添加剩余的数据
				adapter.notifyDataSetChanged(); // 重新渲染界面
				break;
			}
		
		}
	}
	
	/**
	 * OnItemLongClickListener (联系人列表被点击了)
	 */
	public boolean onItemLongClick(AdapterView<?> listView, View itemView,
			int position, long id) {
		currentPosition = position; // 记录当前点击的位置
		
		// 得到被点击那一行对应的  map 数据
		HashMap<String, Object> map = (HashMap<String, Object>) listView.getItemAtPosition(position);
		// 实例化ListView
		ListView view = (ListView) getLayoutInflater().inflate(R.layout.list_view, null);
		// 设置监听器
		view.setOnItemClickListener(this);
		// 得到字符串数组
		String[] options = getResources().getStringArray(R.array.options);
		// 生成适配器  (第二个参数是指TextView所在的布局, 这个布局的根节点一定是TextView)
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.options_item, options);
		// new ArrayAdapter<T>(context, R.layout.options_item, R.id.title, objects)
		// 设置适配器
		view.setAdapter(adapter);
		// 得到姓名
		Object name = map.get(Constants.NAME);
		// 创建对话框
		optionDialog = DialogUtil.createDialog(this, name, view);
		
		this.currentMap = map;
		return false;
	}

	/**
	 * OnItemClickListener (操作对话框的点击)
	 */
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		switch(position) {
		case OPTION_CALL: // 打电话
			String phone = this.currentMap.get(Constants.PHONE).toString();
			Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phone));
			startActivity(intent);
			break;
		case OPTION_SEND: // 发短信
			phone = this.currentMap.get(Constants.PHONE).toString();
			intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"+phone));
			startActivity(intent);
			break;
		case OPTION_EDIT: // 编辑
			intent = new Intent(this, AddContact.class);
			intent.putExtra(CONTACT, this.currentMap);
			startActivityForResult(intent, CODE_ADD_CONTACT); 
			// 希望从"添加联系人"界面回来的时候返回一个结果 
			break;
		case OPTION_DELETE: // 删除
			// 取出联系人的id
			Object contactId = this.currentMap.get(Constants.ID);
			if (ContactMgr.get(this).delete(contactId) > 0) {
				Toast.makeText(this, R.string.delete_success, Toast.LENGTH_SHORT).show();
				
				this.data.remove(this.currentMap); // 删除当前的map数据
				adapter.notifyDataSetChanged();
				
				//loadData(null); // 刷新界面
			} else {
				Toast.makeText(this, R.string.delete_failure, Toast.LENGTH_SHORT).show();
			}
			break;
		}
		
		// 关闭对话框
		if (optionDialog!=null && optionDialog.isShowing()) {
			optionDialog.dismiss();
		}
	}
	
	/**
	 * 监听搜索框
	 * @author Administrator
	 *
	 */
	private class SearchListner implements TextWatcher {
		/**
		 * TextWatcher
		 */
		public void afterTextChanged(Editable s) {
			loadData(s.toString());
		}
		/**
		 * TextWatcher
		 */
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}
		/**
		 * TextWatcher
		 */
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}
	}
	
	/**
	 * RenderListener
	 */
	public void render(ItemModel model) {
		
		List<PackageInfo> infos = getPackageManager().getInstalledPackages(0);
		
		
		ArrayList<PackageInfo>  mySkins = new ArrayList<PackageInfo>();
		for (PackageInfo info : infos) {
			String name = info.packageName;
			if(name.matches("com.mj.pifu_\\w")) {
				mySkins.add(info);
			}
		}
		
		
	}

	/**
	 * Comparator
	 */
	public int compare(HashMap<String, Object> map1,
			HashMap<String, Object> map2) {
		String pinyin1 = map1.get(Constants.PIN_YIN).toString(); // 取出第一个map的全拼
		String title1 = map1.get(Constants.TITLE).toString();  // 取出第一个map的标题
		
		String pinyin2 = map2.get(Constants.PIN_YIN).toString(); // 取出第二个map的全拼
		String title2 = map2.get(Constants.TITLE).toString();   // 取出第二个map的标题
		
		boolean jin1 = ContactMgr.JIN.equals(title1); // 判断第一个map的标题否为为#
		boolean jin2 = ContactMgr.JIN.equals(title2); // 判断第二个map的标题否为为#
		if (jin1 && jin2) { // 如果标题都是#
			return pinyin1.compareTo(pinyin2);
		}
		if (jin1) { // 如果第一个map的标题是#
			return 1;
		}
		if (jin2) { // 如果第二个map的标题是#
			return -1;
		}
		// 如果两个map的标题都不是#
		return pinyin1.compareTo(pinyin2);
	}
}