package gz.itcast.cn.contacts.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import gz.itcast.cn.contacts.R;
import gz.itcast.cn.contacts.mgr.ContactMgr;
import gz.itcast.cn.contacts.model.BaseForm;
import gz.itcast.cn.contacts.model.DateForm;
import gz.itcast.cn.contacts.model.SexForm;
import gz.itcast.cn.contacts.model.TextForm;
import gz.itcast.cn.contacts.util.BitmapUtil;
import gz.itcast.cn.contacts.util.Constants;
import gz.itcast.cn.contacts.util.DialogUtil;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
/**
 * 添加、更新联系人
 * @author Administrator
 *
 */
@SuppressWarnings("unchecked")
public class AddContact extends Activity implements OnClickListener, OnItemClickListener{
	private static final int CAMERA = 0; // 拍照
	private static final int CODE_CAMERA = 0; // 跳到拍照界面的请求码
	private static final int ALBUM = 1; // 相册
	private static final int CODE_ALBUM = 1; // 跳到相册界面的请求码
	private static final int CODE_CROP = 2; // 跳到裁剪界面的请求码
	
	// 存放表单信息的布局
	LinearLayout other;
	// 表单集合
	ArrayList<BaseForm> forms;
	// 姓名
	EditText nameEt;
	// 头像
	ImageView photoImage;
	// 选择头像对话框
	Dialog photoDialog; 
	// 传递过来的联系人数据
	HashMap<String, Object> contact;
	
	Bitmap bitmap; // 拍照或者从相册中选取后的图片
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.add_contact);
		other = (LinearLayout) findViewById(R.id.other_info);
		forms = new ArrayList<BaseForm>();
		
		// 姓名
		nameEt = (EditText) findViewById(R.id.name);
		// 姓名
		TextForm nameForm = new TextForm(this, null, Constants.NAME);
		nameForm.require = true;
		nameForm.editText = nameEt;
		nameForm.label = getString(R.string.name);
		forms.add(nameForm);
		
		// 地址
		TextForm addressForm = new TextForm(this, R.string.address, Constants.ADDRESS);
		addressForm.require = true;
		addForm(1, addressForm);
		
		// 电话
		TextForm phoneForm = new TextForm(this, R.string.phone, Constants.PHONE);
		phoneForm.require = true;
		addForm(1, phoneForm);
		phoneForm.editText.setInputType(InputType.TYPE_CLASS_NUMBER);
		
		// 邮件
		TextForm emailForm = new TextForm(this, R.string.email, Constants.EMAIL);
		addForm(1, emailForm);
		emailForm.editText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		
		// 性别
		SexForm sexForm = new SexForm(this, R.string.sex, Constants.SEX);
		addForm(0, sexForm);
		
		// 生日
		DateForm birthdayForm = new DateForm(this, R.string.birthday, Constants.BIRTHDAY);
		addForm(0, birthdayForm);
		
		// 按钮
		Button btnSave = (Button) findViewById(R.id.btn_save);
		btnSave.setOnClickListener(this);
		Button btnCancel = (Button) findViewById(R.id.btn_cancel);
		btnCancel.setOnClickListener(this);
		
		// 头像
		photoImage = (ImageView) findViewById(R.id.photo);
		photoImage.setOnClickListener(this);
		
		
		Bundle bundle = getIntent().getExtras();
		TextView titleView = (TextView)findViewById(R.id.title);
		// 设置标题
		if (bundle == null) {
			titleView.setText(R.string.add_contact);
		} else {
			titleView.setText(R.string.update_contact);
			
			HashMap<String, Object> map = (HashMap<String, Object>) bundle.get(Contacts.CONTACT);
			setData(map); // 设置数据
			this.contact = map;
		}
	}
	
	/**
	 * 设置表单数据
	 */
	private void setData(HashMap<String, Object> map) {
		// 设置头像
		String path = map.get(Constants.PHOTO).toString();
		photoImage.setImageBitmap(BitmapUtil.getBitmap(path));
		
		// 设置其他表单内容
		for (BaseForm form : forms) {
			String key = form.column;
			form.setValue(map.get(key));
		}
	}
	
	/**
	 * 添加一个表单
	 * @param index
	 */
	private void addForm(int index, BaseForm form) {
		forms.add(form);

		LinearLayout section = null; 
		LinearLayout other = this.other;
			
		while (other.getChildCount() < index + 1) { // 如果区域的数目不够
			other.addView(   section = createSection()  ); //创建新的区域， 添加区域
		} 
		
		if (section == null) { // 如果没有进入while循环, 就从other里面取得section
			section = (LinearLayout) other.getChildAt(index); // 取出相应的区域
		}
		
		if (section.getChildCount()>0) { // 如果前面已经有表单, 就要先添加一个分隔线
			section.addView(getDivider(this));
		}
		section.addView(form.createView());
	}
	
	/**
	 * 生成一块区域
	 * @return
	 */
	private LinearLayout createSection() {
		LinearLayout section = new LinearLayout(this);
		// 设置布局参数
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		params.bottomMargin = 20;
		section.setLayoutParams(params);
		section.setBackgroundResource(R.drawable.other_info_bg); // 设置背景
		section.setOrientation(LinearLayout.VERTICAL); // 设置方向
		return section;
	}
	
	/**
	 * 創建分割線
	 */
	private static View getDivider(Context context) {
		View divider = new View(context);
		// 设置分隔线背景
		divider.setBackgroundColor(context.getResources().getColor(R.color.form_divider));
		// 分隔线的布局参数
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT, 2);
		divider.setLayoutParams(params);
		return divider;
	}

	/**
	 * OnClickListener:监听控件的点击事件
	 */
	public void onClick(View v) {
		if (v.getId() == R.id.btn_save) { // 保存
			ContentValues values = new ContentValues();
			
			// 头像
			if (uri != null) { // 说明选取过相片
				values.put(Constants.PHOTO, uri.getPath());
			} else if (contact != null) { // 说明更新, 而且没有选取相片
				values.put(Constants.PHOTO, contact.get(Constants.PHOTO).toString());
			}
			
			// 遍历表单信息
			for (BaseForm form : forms) {
				// 得到表单值
				String value = form.getValue().toString();
				// 判断有无填写必填的表单
				if (form.require && value.trim().length()==0) {
					Toast.makeText(this, "请输入"+form.label, Toast.LENGTH_SHORT).show();
					return;
				}
				values.put(form.column, value);
			}
			
			long ret = -1;
			
			Intent intent = new Intent();
			
			if (contact == null) { // 添加
				// 返回到Contacts界面, 声明是"添加"
				intent.putExtra(Contacts.ADD, true);
				// 添加到数据库
				ret = ContactMgr.get(this).add(values);
				// 装载id
				values.put(Constants.ID, ret);
			} else {
				// 装载id
				values.put(Constants.ID, contact.get(Constants.ID).toString());
				// 更新到数据库
				ret = ContactMgr.get(this).update(values);
			}
			
			if (ret > 0) { // 保存成功
				Toast.makeText(this, R.string.save_success, Toast.LENGTH_SHORT).show();
				
				intent.putExtra(Contacts.CONTACT, values);
				setResult(Contacts.CODE_REFRESH, intent); // 返回一个结果值, 告诉contacts要刷新
			} else { // 保存失败
				Toast.makeText(this, R.string.save_failure, Toast.LENGTH_SHORT).show();
			}
			
			finish();
		} else if (v.getId() == R.id.btn_cancel) { // 取消
			finish();
		} else if (v.getId() == R.id.photo) { // 选择头像
			// 实例化ListView
			ListView view = (ListView) getLayoutInflater().inflate(R.layout.list_view, null);
			// 设置监听器
			view.setOnItemClickListener(this);
			// 得到字符串数组
			String[] options = getResources().getStringArray(R.array.photo);
			// 生成适配器  (第二个参数是指TextView所在的布局, 这个布局的根节点一定是TextView)
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.options_item, options);
			// 设置适配器
			view.setAdapter(adapter);
			
			photoDialog = DialogUtil.createDialog(this, R.string.select_head, view);
		}
	}
	
	Uri uri;
	
	/**
	 * OnItemClickListener
	 */
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = null;
		
		switch (position) {
		case CAMERA: // 拍照
			intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); 
			intent.putExtra(MediaStore.EXTRA_OUTPUT, uri = getUri());
			startActivityForResult(intent, CODE_CAMERA);
			break;

		case ALBUM: // 相册
			intent = new Intent(Intent.ACTION_GET_CONTENT); 
			intent.setType("image/*"); // 要取的资源:图片资源
			// intent.putExtra("return-data", true);
			
			//要裁剪
			intent.putExtra("crop", "true");
			//裁剪比例
			intent.putExtra("aspectX", 1);
			//裁剪比例
			intent.putExtra("aspectY", 1);
			
			intent.putExtra(MediaStore.EXTRA_OUTPUT, uri = getUri());
			startActivityForResult(intent, CODE_ALBUM);
			break;
		}
		
		photoDialog.dismiss();
	}
	
	private Uri getUri() {
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			//  sdPath: /mnt/sdcard
			String sdPath = Environment.getExternalStorageDirectory().getAbsolutePath();
			
			// contactsPath: /mnt/sdcard/contacts
			String contactsPath = sdPath + File.separator + "contacts";
			File contactsFile = new File(contactsPath);
			if (!contactsFile.exists()) {
				contactsFile.mkdirs();
			}
			
			// imagesPath: /mnt/sdcard/contacts/images
			String imagesPath = contactsPath + File.separator + "images";
			File imagesFile = new File(imagesPath);
			if (!imagesFile.exists()) {
				imagesFile.mkdirs();
			}
			
			//  /mnt/sdcard/contacts/images/321432423432.jpg
			return Uri.fromFile(new File(imagesPath, System.currentTimeMillis() + ".jpg"));
		} else {
			Toast.makeText(this, "内存卡不存在", Toast.LENGTH_SHORT).show();
			return null;
		}
		
	}
	
	/**
	 * 从其他界面回来
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CODE_CAMERA) { // 拍照回来, 手动跳到裁减界面
			Intent intent = new Intent("com.android.camera.action.CROP");
			intent.setDataAndType(uri, "image/*"); // 需要裁减的图片
			//裁剪比例
			intent.putExtra("aspectX", 1);
			//裁剪比例
			intent.putExtra("aspectY", 1);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, uri); // 裁剪后的输出
			startActivityForResult(intent, CODE_CROP);
		} else if (requestCode == CODE_ALBUM || requestCode == CODE_CROP) { // 从相册裁剪图片回来
			// 取出所拍的照片
			//Bitmap bitmap = data.getParcelableExtra("data");
			//photoImage.setImageBitmap(bitmap);
			
			// 回收上次的图片资源
			if (bitmap != null && !bitmap.isRecycled()) {
				bitmap.recycle(); // 回收
				bitmap = null; 
			}
			
			// OOM (Out Of Memory, 内存溢出)
			// ANR (Application Not Response, 程序未响应)
			
			String path = uri.getPath(); // 得到图片路径 
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = 2; // 加载上来的图片的 宽度和高度是原图的 1/2, 也就是原图的1/4
			// 这个options是为了节省内存支出
			bitmap  = BitmapFactory.decodeFile(path, options);
			photoImage.setImageBitmap(bitmap);
			// photoImage.setImageURI(uri);
		}
	}
}
