package gz.itcast.cn.contacts.util;

/**
 * 常量
 * @author Administrator
 *
 */
public class Constants {
	public static final String TABLE = "t_contact"; // 表名
	
	public static final String ID = "id"; // 主键
	public static final String PHOTO = "photo"; // 照片
	public static final String NAME = "name"; // 姓名
	public static final String SEX = "sex"; // 性别
	public static final String ADDRESS = "address"; // 地址
	public static final String PHONE = "phone"; // 手机
	public static final String EMAIL = "email"; // 邮件
	public static final String BIRTHDAY = "birthday"; // 生日
	public static final String PIN_YIN = "pin_yin"; // 全拼
	public static final String PIN_YIN_HEAD = "pin_yin_head"; //声母
	public static final String TITLE = "title"; // 标题(首大写字母)
}
