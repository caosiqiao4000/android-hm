package gz.itcast.cn.contacts.util;

public interface Callback<T> {
	public void execute(T t)  throws Exception;
}
