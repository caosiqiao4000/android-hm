package gz.itcast.cn.contacts.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BitmapUtil {
	private static BitmapFactory.Options options;
	
	static {
		options = new BitmapFactory.Options(); // 这个options是为了节省内存支出
		options.inSampleSize = 8; // 加载上来的图片的 宽度和高度是原图的 1/8, 也就是原图的1/64
	}
	
	/**
	 * 根据图片路径, 加载对应的图片
	 * @param path
	 * @return
	 */
	public static Bitmap getBitmap(String path) {
		return BitmapFactory.decodeFile(path, options);
	}
}
