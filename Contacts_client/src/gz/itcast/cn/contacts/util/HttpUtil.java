package gz.itcast.cn.contacts.util;

import gz.itcast.cn.contacts.R;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

public class HttpUtil {
	
	public static void post(final Context context,final  String uri, final HashMap<String, Object> form, final Callback callback) {
		
		new AsyncTask<Object, Object, JSONObject>(){
			@Override
			protected JSONObject doInBackground(Object... params) {
				try {
					HttpClient client = new DefaultHttpClient();
					// http://192.168.10.230:8080/contact_server/
					String baseUrl = context.getString(R.string.base_url);
					// 初始化请求
					HttpPost post = new HttpPost(baseUrl + uri);
					// 用来装载表单参数
					MultipartEntity entity  = new MultipartEntity();
					
					Set<Entry<String, Object>> set = form.entrySet();
					for(Entry<String, Object> entry : set) {
							// 装载表单参数
							entity.addPart(entry.getKey(), new StringBody(entry.getValue().toString()));
					}
					
					post.setEntity(entity);
					// 发送请求
					HttpResponse response = client.execute(post);
					
					if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						// 得到服务器端返回数据的封装对象
						HttpEntity e = response.getEntity();
						String result = EntityUtils.toString(e);
						
						System.out.println(result);
						
						
						return new JSONObject(result);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void onPostExecute(JSONObject json) {
				if (callback!=null) {
					try {
						callback.execute(json);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}.execute(new Object[]{});
		
	}
	
}
