package gz.itcast.cn.contacts.util;

import gz.itcast.cn.contacts.R;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class DialogUtil {
	/**
	 * ��������
	 * @param param
	 */
	private static String parseParam(Context context, Object param) {
		if (param instanceof Integer) {
			return context.getString((Integer)param);
		} else if (param instanceof String) {
			return param.toString();
		}
		return null;
	}
	
	
	/**
	 * 
	 * @param context
	 * @param titleText
	 * @param contentText
	 * @param leftText
	 * @param rightText
	 * @param listener
	 * @return
	 */
	public static Dialog createDialog(Context context , Object titleText, Object contentText, 
			final Object leftText , final Object rightText, final MyDialogListener listener, boolean force) {
		View view = LayoutInflater.from(context).inflate(R.layout.dialog, null);
		
		//AlertDialog.Builder builder = new AlertDialog.Builder(context);
		final Dialog dialog = new Dialog(context, R.style.MyDialog);
		// show��ʱ������requestFeature()
		dialog.show();
		// adding content
		dialog.setContentView(view);
		//dialog.getWindow().setContentView(view);
		
		// ����
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText(parseParam(context, titleText));
		
		// ����
		TextView content = (TextView) view.findViewById(R.id.content);
		
		if (contentText instanceof View) { // һ��View
			ViewGroup parent = (ViewGroup) content.getParent();
			// ɾ��TextView
			parent.removeView(content);
			parent.addView((View) contentText);
			
		} else { // ����������
			content.setText(parseParam(context, contentText));
		}
		
		
		
		
		// ���þֲ������ķ���
		Button left = (Button) view.findViewById(R.id.left);
		
		if (leftText == null) { // ����Ҫ��ť
			// �ҵ���ť���ڵĲ���
			ViewGroup btnsLayout = (ViewGroup) left.getParent(); 
			// �õ���һ��
			ViewGroup parent = (ViewGroup) btnsLayout.getParent();
			parent.removeView(btnsLayout);
			
		} else { // ��Ҫ��ť
			left.setText(parseParam(context, leftText));
			left.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					listener.onClick(dialog, (Button)v, leftText);
				}
			});
			
			Button right = (Button) view.findViewById(R.id.right);
			
			// ˵��ֻ��Ҫһ����ť
			if (rightText == null) {
				// �õ����ڵ�
				ViewGroup parent = (ViewGroup) right.getParent();
				parent.removeView(right);
			} else { // ��Ҫ�ұߵİ�ť
				right.setText(parseParam(context, rightText));
				
				// ǿ�ư�
				if (force) {
					right.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							listener.onClick(dialog, (Button)v, rightText);
						}
					});
				} else { // Ĭ�ϵļ�����
					right.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							dialog.dismiss();
						}
					});
				}
			}
		}
		return dialog;
	}
	
	/**
	 * ���ұߵİ�ť��Ĭ�ϵļ�����
	 */
	public static Dialog createDialog(Context context , Object titleText, Object contentText, 
			final Object leftText , final Object rightText, final MyDialogListener listener) {
		return createDialog(context, titleText, contentText, leftText, rightText, listener, false);
	}
	
	/**
	 * û�а�ť
	 */
	public static Dialog createDialog(Context context , Object titleText, Object contentText) {
		return createDialog(context, titleText, contentText, null, null, null, false);
	}
	
	/**
	 * ������ť�ķ���
	 */
	public static Dialog createDialog(Context context , Object titleText, Object contentText, 
			final Object btnText , final MyDialogListener listener) {
		return createDialog(context, titleText, contentText, btnText, null, listener, false);
	}
	
	public interface MyDialogListener {
		void onClick(Dialog dialog, Button btn, Object btnKey);
	}
}
