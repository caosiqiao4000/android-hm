package gz.itcast.cn.contacts.model;

import android.content.Context;
import android.view.View;

/**
 * 对表单信息的基本封装
 * @author Administrator
 *
 */
public abstract class BaseForm {
	public String label;  // 标签
	public String column; // 字段
	public Context context;
	public boolean require; // 是否必须
	
	public BaseForm(Context context, Object labelObject, String column) {
		this.context = context;
		this.label = parse(labelObject);
		this.column = column;
	}
	
	/********************都交给子类去实现*****************************/
	/**
	 * 得到表单当前的值
	 */
	public abstract Object getValue();
	/**
	 * 创建对应的表单界面
	 */
	public abstract View createView();
	/**
	 * 设置表单的值
	 */
	public abstract void setValue(Object value);
	/********************都交给子类去实现*****************************/
	
	/**
	 * 解析参数
	 */
	protected String parse(Object param) {
		if (param instanceof Integer) {
			return context.getString((Integer)param);
		} else if (param instanceof String) {
			return param.toString();
		}
		return null;
	}
}
