package gz.itcast.cn.contacts.model;

import gz.itcast.cn.contacts.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
/**
 * 基本的文本框表单
 * @author Administrator
 *
 */
public class TextForm extends BaseForm {
	public EditText editText;
	
	public TextForm(Context context, Object label, String column) {
		super(context, label, column);
	}
	
	public Object getValue() {
		return editText.getText().toString();
	}

	public View createView() {
		View view = LayoutInflater.from(context).inflate(R.layout.text_form, null);
		TextView labelView = (TextView) view.findViewById(R.id.label);
		labelView.setText(label);
		
		EditText editText = (EditText) view.findViewById(R.id.value);
		// 设置提示文字
		String hint = "请输入"+label;
		if (require) {
			hint += "(*)";
		}
		editText.setHint(hint);
		
		this.editText = editText;
		return view;
	}

	public void setValue(Object value) {
		editText.setText(parse(value));
	}
}
