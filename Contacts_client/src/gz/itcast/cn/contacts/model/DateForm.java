package gz.itcast.cn.contacts.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import gz.itcast.cn.contacts.R;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.TextView;
/**
 * 日期控件表单
 */
public class DateForm extends BaseForm implements OnClickListener, OnDateSetListener {
	public TextView textView;
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	public DateForm(Context context, Object labelObject, String column) {
		super(context, labelObject, column);
	}

	public View createView() {
		View view = LayoutInflater.from(context).inflate(R.layout.date_form, null);
		TextView labelView = (TextView) view.findViewById(R.id.label);
		labelView.setText(label);
		this.textView = (TextView) view.findViewById(R.id.value);
		this.textView.setOnClickListener(this);
		this.textView.setText(format.format(new Date())); // 默认为今天的时间
		return view;
	}

	public Object getValue() {
		return textView.getText().toString();
	}

	public void setValue(Object value) {
		if (value == null || value.toString().trim().length() == 0) {
			this.textView.setText(format.format(new Date())); // 默认为今天的时间
		} else {
			textView.setText(parse(value));
		}
	}

	/**
	 * OnClickListener
	 */
	public void onClick(View v) {
		try {
			Date date = format.parse(getValue().toString());// 得到textView上面的时间
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date); // 设置时间
			// 得到对应的年月日
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH);
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			// 弹出选择日期的对话框
			DatePickerDialog dialog = new DatePickerDialog(context, this, year, month, day);
			dialog.show();
		} catch (ParseException e) {
			e.printStackTrace();
		} 
	}

	/**
	 * OnDateSetListener
	 */
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, monthOfYear, dayOfMonth); // 设置选择的时间
		setValue(format.format(calendar.getTime())); // 设置TextView的值
	}

}
