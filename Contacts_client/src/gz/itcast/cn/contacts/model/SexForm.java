package gz.itcast.cn.contacts.model;

import gz.itcast.cn.contacts.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * 选取性别的表单(男，女)
 * @author Administrator
 *
 */
public class SexForm extends BaseForm {
	public RadioGroup group;
	
	public SexForm(Context context, Object label, String column) {
		super(context, label, column);
	}

	public Object getValue() {
		int checkId = group.getCheckedRadioButtonId();
		if (checkId == R.id.man) {
			return 0;
		} else if (checkId == R.id.woman) {
			return 1;
		}
		return null;
	}

	public View createView() {
		View view = LayoutInflater.from(context).inflate(R.layout.sex_form, null);
		RadioGroup group = (RadioGroup) view.findViewById(R.id.group);
		// 默认选中"男"
		((RadioButton)group.findViewById(R.id.man)).setChecked(true);
		
		this.group = group;
		return view;
	}

	public void setValue(Object value) {
		if (value == null) value = 0;
		
		RadioGroup group = this.group;
		if (value.equals(0)) {
			((RadioButton)group.findViewById(R.id.man)).setChecked(true);
		} else {
			((RadioButton)group.findViewById(R.id.woman)).setChecked(true);
		}
	}
}
