package gz.itcast.cn.contacts.mgr;

import gz.itcast.cn.contacts.R;
import gz.itcast.cn.contacts.db.DBHelper;
import gz.itcast.cn.contacts.util.Constants;
import gz.itcast.cn.contacts.util.PinyinUtil;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * 封装了对联系人的业务操作
 * @author Administrator
 *
 */
public class ContactMgr {
	public static final String JIN = "#";
	private static ContactMgr mgr;
	
	SQLiteDatabase db;
	
	private ContactMgr(Context context) {
		db = new DBHelper(context).getWritableDatabase();
	}
	
	public static ContactMgr get(Context context) {
		if (mgr == null) {
			mgr = new ContactMgr(context);
		}
		return mgr;
	}
	
	/**
	 * 添加或者更新后的调整
	 * @param values
	 */
	private void fixContact(ContentValues values) {
		String name = values.getAsString(Constants.NAME); // 取出姓名
		values.put(Constants.PIN_YIN, PinyinUtil.getPinYin(name));  // 全拼
		
		String shengmu = PinyinUtil.getPinYinHeadChar(name);
		values.put(Constants.PIN_YIN_HEAD, shengmu);  // 声母
		
		// 得到大写的首字母
		String capital = shengmu.substring(0, 1).toUpperCase();
		if (PinyinUtil.isBigCapital(capital)) {
			values.put(Constants.TITLE, capital);  // 大写字母
		} else {
			values.put(Constants.TITLE, JIN);  // 大写字母
		}
	}
	
	/**
	 * 添加联系人
	 * @param values
	 * @return
	 */
	public long add(ContentValues values) {
		Object photo = values.get(Constants.PHOTO);
		if (photo == null) { // 如果添加的时候没有选择头像, 那就使用默认的头像
			values.put(Constants.PHOTO, R.drawable.default_photo);
		}
		
		fixContact(values); // 对联系人信息进行修正
		
		return db.insert(Constants.TABLE, null, values);
	}
	
	/**
	 * 删除联系人
	 * @param id
	 * @return
	 */
	public int delete(Object id) {
		if (id == null) return -1;
		return db.delete(Constants.TABLE, Constants.ID+"=?", 
				new String[]{id.toString()});
	}
	
	public int deleteAll(ArrayList<Object> ids) {
		int ret = 0;
		try {
			for (Object id : ids) {// 一个一个地删除联系人数据
				ret += delete(id);
			}
		} catch (Exception e) {
			return ret;
		}
		return ret;
	}
	
	/**
	 * 更新联系人
	 * @param values
	 * @return
	 */
	public int update(ContentValues values) {
		fixContact(values);// 对联系人信息进行修正
		return db.update(Constants.TABLE, values, Constants.ID + "=?", 
				new String[]{values.getAsString(Constants.ID)});
	}
	
	/**
	 * 通过名字来查询
	 * @param condition
	 * @return
	 */
	public ArrayList<HashMap<String, Object>> find(String condition) {
		if (condition == null) condition = "";
		
		String selection = Constants.NAME+" like ? ";
		String[] args = new String[]{"%"+condition+"%"};
		String orderBy = Constants.TITLE + " asc ";
		
		Cursor cursor = db.query(Constants.TABLE, null, selection, args, null, null, orderBy);
		return getList(cursor);
	}
	
	private static ArrayList<HashMap<String, Object>> getList(Cursor cursor) {
		// 所有的联系人数据
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String,Object>>();
		// #标题的联系人数据
		ArrayList<HashMap<String, Object>> jinList = new ArrayList<HashMap<String,Object>>();
		
		while (cursor.moveToNext()) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			// 装载基本信息
			map.put(Constants.ID, cursor.getInt(cursor.getColumnIndex(Constants.ID)));
			map.put(Constants.PHOTO, cursor.getString(cursor.getColumnIndex(Constants.PHOTO)));
			map.put(Constants.SEX, cursor.getInt(cursor.getColumnIndex(Constants.SEX)));
			map.put(Constants.NAME, cursor.getString(cursor.getColumnIndex(Constants.NAME)));
			map.put(Constants.BIRTHDAY, cursor.getString(cursor.getColumnIndex(Constants.BIRTHDAY)));
			map.put(Constants.ADDRESS, cursor.getString(cursor.getColumnIndex(Constants.ADDRESS)));
			map.put(Constants.PHONE, cursor.getString(cursor.getColumnIndex(Constants.PHONE)));
			map.put(Constants.EMAIL, cursor.getString(cursor.getColumnIndex(Constants.EMAIL)));
			
			// 标题
			String title = cursor.getString(cursor.getColumnIndex(Constants.TITLE));
			map.put(Constants.TITLE, title);
			// 全拼
			map.put(Constants.PIN_YIN, cursor.getString(cursor.getColumnIndex(Constants.PIN_YIN)));
			
			// 如果是#标题
			if (JIN.equals(title)) {
				jinList.add(map);
			} else {
				list.add(map);
			}
		}
		
		list.addAll(jinList); // 将#标题的联系人数据加到最后面
		return list;
	}
	
	/**
	 * 通过拼音来搜索
	 * @param condition : mj
	 */
	public ArrayList<HashMap<String, Object>> findByPinyin(String condition) {
		if (condition == null) condition = "";
		
		String selection = Constants.PIN_YIN_HEAD + " like ? ";
		String[] args = new String[]{"%"+condition+"%"};
		String orderBy = Constants.TITLE + " asc ";
		
		Cursor cursor = db.query(Constants.TABLE, null, selection, args, null, null, orderBy);
		return getList(cursor);
	}
}
