package gz.itcast.cn.contacts.adapter;

import gz.itcast.cn.contacts.R;
import gz.itcast.cn.contacts.util.BitmapUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

/**
 * 自定义Adapter
 * @author Administrator
 *
 */
@SuppressWarnings("unchecked")
public class MyAdapter extends SimpleAdapter {
	private HashMap<Integer, Bitmap> bitmaps;
	
	private static final int DELTA = 3;
	
	public String[] from;
	public int[] to;
	public int resource;
	public Context context;
	public ArrayList<HashMap<String, Object>> data;
	
	public ListView listView; // 对应的ListView
	
	public RenderListener listener;
	
	public MyAdapter(Context context, List<? extends Map<String, ?>> data,
			int resource, String[] from, int[] to, RenderListener listener) {
		super(context, data, resource, from, to);
		this.from = from;
		this.to = to;
		this.resource = resource;
		this.data = (ArrayList<HashMap<String, Object>>) data;
		this.context = context;
		this.listener = listener;
		
		this.bitmaps = new HashMap<Integer, Bitmap>();
	}
	
	/**
	 * 实例化position对应的item, 当一个item可视的时候调用
	 */
	public View getView(final int position, View convertView, ViewGroup parent) {
		ItemModel model = null;
		
		if (convertView == null) { // 没有可利用的ItemView
			// 实例化item布局
			convertView = LayoutInflater.from(this.context).inflate(this.resource, null);
			model = new ItemModel(); // 初始化model
			convertView.setTag(model); // 绑定ItemView跟ItemModel
			model.itemView = convertView; // 设置ItemModel的ItemView
		} else {
			model = (ItemModel) convertView.getTag(); // 从ItemView中取出对应的ItemModel
		}
		
		model.position = position;
		
		// 得到对应的map数据
		HashMap<String, Object> map = this.data.get(position); 
		model.map = map;
		
		int length = this.to.length;
		// 装配数据, 覆盖以前的数据
		for (int i = 0; i < length; i++) {
			int resId = to[i];
			
			View temp = model.children.get(resId);
			if (temp == null) { // 如果之前没有find过子控件
				temp = convertView.findViewById(resId); //得到子控件
				// 放入对应的子控件
				model.children.put(resId, temp);
			}
			
			final View childView = temp; // 直接从model中取出子控件
			
			Object value = null; // 得到对应的value
			if (i < from.length) {
				String key  = from[i]; // 得到对应的key
				value = map.get(key);
			}
			
			
			// 设置数据 (子类要在前面)
			Class clazz = childView.getClass();
			if (clazz.equals(TextView.class)) { // 如果是TextView, 就设置文字
        		((TextView)childView).setText(value.toString());
        	} else if (clazz.equals(ImageView.class)) { // 如果是ImageView, 就设置图片
        		String path = null;
        		if (value != null && (path = value.toString()).trim().length()>0) { // 如果有路径
        			
        			// 加载图片之前  先给个默认的背景图片
        		    ((ImageView)childView).setImageResource(R.drawable.default_photo);
        			
        			/**
        			 * 第一个泛型:doInBackground的形参， execute的实参
        			 * (execute中的实参        会赋值给     doInBackground的形参)
        			 * 
        			 * 第二个泛型:onProgressUpdate的形参
        			 * 
        			 * 第三个泛型:onPostExecute的形参, doInBackground的返回值
        			 * doInBackground的返回值        会赋值给       onPostExecute的形参
        			 */
        			new AsyncTask<String, Integer, Bitmap>() {

						/**
						 * 在后台线程调用这个方法
						 */
						protected Bitmap doInBackground(String... params) {
							// TODO
							clearTempImages(); // 清理临时图片
							
							String path = params[0]; // 得到路径
							
							Bitmap bitmap =  BitmapUtil.getBitmap(path); // 根据路径 加载图片, 加载比较耗时
							//bitmaps.get(position);
                			if (bitmap == null) { // 如果从未加载过图片
                				bitmap = BitmapUtil.getBitmap(path); // 根据路径 加载图片, 加载比较耗时
                				bitmaps.put(position, bitmap);
                			}
							return bitmap;
						}

						/**
						 * 回到主线程调用这个方法
						 */
						protected void onPostExecute(Bitmap bitmap) {
							// 第一个可视化item的位置
							int firstPosition = listView.getFirstVisiblePosition();
							// 最后一个可视化item的位置
							int lastPosition = listView.getLastVisiblePosition();
							
							/**
							 * 当你回来设置图片的时候, 要判断一下你的item是否在可视范围内
							 */
							if (
								(position <=lastPosition && position>=firstPosition)
									&& 
									(bitmap!=null && !bitmap.isRecycled())) {
								((ImageView)childView).setImageBitmap(bitmap);
							}
							
						}
        			}.execute(path);
        		}
        	}
		}
		
		System.out.println("size:--------"+bitmaps.size());
		
		if (listener != null) { // 如果有监听器
			listener.render(model);
		}
		return convertView;
	}
	
	/**
	 * 清除临时的Bitmap
	 */
	private void clearTempImages() {
		// 第一个可视化item的位置
		int firstPosition = listView.getFirstVisiblePosition();
		
		// 最后一个可视化item的位置
		int lastPosition = listView.getLastVisiblePosition();
		
		// 清理前面的bitmap
		int beforeIndex = firstPosition - DELTA;
		for (int i = 0; i < beforeIndex; i++) {
			freeBitmap(i);
		}
		
		
		// 清理后面的bitmap
		int afterIndex = lastPosition + DELTA;
		int size = data.size();
		for (int i = afterIndex; i < size; i++) {
			freeBitmap(i);
		}
	}
	
	/**
	 * 释放某个index对应的Bitmap
	 * @param index
	 */
	private void freeBitmap(int index) {
		Bitmap bitmap = bitmaps.get(index);
		if (bitmap != null && !bitmap.isRecycled()) {
			bitmap.recycle(); // 回收
			bitmap = null;
			bitmaps.remove(index); // 从map中清除
		}
	}
	
	
	/**
	 * Item的渲染监听器
	 */
	public interface RenderListener {
		void render(ItemModel model);
	}
	
	/**
	 * 对item的数据封装
	 */
	public class ItemModel {
		public HashMap<String, Object> map; // 对应的map数据
		public HashMap<Integer, View> children = new HashMap<Integer, View>(); // 里面的子控件
		public int position; // 所在的位置
		public View itemView; // 对应的ItemView
		
		/**
		 * 根据资源id得到对应的子控件
		 */
		public <T extends View>T getChild(int resId) {
			return (T) children.get(resId);
		}
		
		/**
		 * 根据key来获取对应的数据
		 */
		public Object getMapValue(String key) {
			return map.get(key);
		}
	}
}
