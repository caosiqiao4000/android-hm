package gz.itcast.cn.contacts.db;

import gz.itcast.cn.contacts.util.Constants;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 访问、操作数据库的工具类
 * @author Administrator
 *
 */
public class DBHelper extends SQLiteOpenHelper {
	private static final String DB_NAME = "1205.db";
	private static final int DB_VERSION = 5;
	
	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		StringBuffer sb = new StringBuffer("create table ").append(Constants.TABLE).append("(")
		.append(Constants.ID).append(" integer primary key autoincrement, ")
		.append(Constants.PHOTO).append(" text, ")
		.append(Constants.NAME).append(" text, ")
		.append(Constants.PHONE).append(" text, ")
		.append(Constants.PIN_YIN).append(" text, ")
		.append(Constants.PIN_YIN_HEAD).append(" text, ")
		.append(Constants.BIRTHDAY).append(" text, ")
		.append(Constants.TITLE).append(" text, ")
		.append(Constants.ADDRESS).append(" text, ")
		.append(Constants.EMAIL).append(" text, ")
		.append(Constants.SEX).append(" integer );");
		
		db.execSQL(sb.toString());
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("drop table if exists "+Constants.TABLE+" ;");
		onCreate(db);
	}
}
